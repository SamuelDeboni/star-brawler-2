internal void
start_boss(bool enable) {
    gs->boss = {};

	{ // Terminals
		gs->boss.terminals.x_position = 300;

		gs->boss.terminals.top_terminal_y = 240;
		gs->boss.terminals.top_terminal_collider.type   = DShapeType_Rect;
		gs->boss.terminals.top_terminal_collider.r.size = vec2(50, 25);
		gs->boss.terminals.top_terminal_sprite = gs->terminals_sprite;
		play_animation(&gs->boss.terminals.top_terminal_sprite, 0, true);

		gs->boss.terminals.bottom_terminal_y = 11;
		gs->boss.terminals.bottom_terminal_collider.type   = DShapeType_Rect;
		gs->boss.terminals.bottom_terminal_collider.r.size = vec2(50, 25);
		gs->boss.terminals.bottom_terminal_sprite = gs->terminals_sprite;
		play_animation(&gs->boss.terminals.bottom_terminal_sprite, 1, true);

		gs->boss.terminals.hp = 40;

		gs->boss.terminals.laser_body_sprite = gs->great_plasma_laser_sprite;
		gs->boss.terminals.laser_collider.type = DShapeType_Rect;
		gs->boss.terminals.laser_collider.r.size = vec2(50, 25);

		gs->boss.terminals.body_damage  = 40;
		gs->boss.terminals.laser_damage = 40;

		gs->boss.terminals.delay=0;
	}

    if (enable) {
        gs->boss.boss_core_id = spawn_boss_core();
    }

	gs->boss.enabled = enable;
}

internal void
make_terminal_pursuit_ship(BossTerminals *t) {
	bool  positive_movement  = t->x_position < gs->ship.position.x;
	float amount_of_movement = (80 * (positive_movement ? 1 : -1)) * delta;

	float diff = t->x_position - gs->ship.position.x;
	if (fabs(amount_of_movement) > fabs(diff)) {
		t->x_position = gs->ship.position.x;
	} else {
		t->x_position += amount_of_movement;
	}
}

internal bool
check_and_damage_terminals(DShape pc, float damage) {
    if (!gs->boss.enabled || gs->boss.stage != 0) return false;

    BossTerminals *t = &gs->boss.terminals;

    t->top_terminal_collider.r.position    = vec2(t->x_position, t->top_terminal_y);
    t->bottom_terminal_collider.r.position = vec2(t->x_position, t->bottom_terminal_y);


    if (t->hit_cooldown <= 0 && (collision(t->bottom_terminal_collider, pc) || collision(t->top_terminal_collider, pc))) {
        t->hp -= damage;
        if(gs->ship.powerup_active_cooldown <= 0){
            gs->ship.energy += 2;
        }
        t->hit_cooldown = 0.15;
        if (t->hp <= 0) {
            create_random_particles(20, vec2(t->x_position, t->top_terminal_y), vec2(5,5), vec2(30,30), WHITE, LIGHTGRAY, true, vec2(5,5));
            create_random_particles(20, vec2(t->x_position, t->bottom_terminal_y), vec2(5,5), vec2(30,30), WHITE, LIGHTGRAY, true, vec2(5,5));
            spawn_upper_cannon(vec2(450-90 ,125+50), 1);
            spawn_center_cannon(vec2(450-113 ,125), 1);
            spawn_lower_cannon(vec2(450-90 ,125-50), 1);
            gs->boss.stage = 1;
        }
        return true;
    }

    return false;
}

internal void
update_terminal() {
	BossTerminals *t = &gs->boss.terminals;
	
	switch (t->state) {
	
	case BossTerminalsState_Initial: {
		make_terminal_pursuit_ship(t);

		t->timer += delta;
		if (t->timer > 0.5) {
			t->timer = 0;
			t->laser_warning_blinks = 0;
			t->state = BossTerminalsState_Warning;
			play_animation(&t->laser_body_sprite, 2, false);
		}
	} break;

	case BossTerminalsState_Warning: {
		make_terminal_pursuit_ship(t);

		if (t->laser_body_sprite.has_finished) {
			t->laser_warning_blinks += 1;
			if (t->laser_warning_blinks >= 3) {
				t->activations_qnt += 1;
				if (t->activations_qnt > 3) {
					t->smash_y_target = (t->top_terminal_y - t->bottom_terminal_y) / 2 + 12;
					t->timer = 0;
					t->state = BossTerminalsState_Smashing;
				} else {
					t->timer = 0;
					t->state = BossTerminalsState_Laser;
					play_animation(&t->laser_body_sprite, 3, true);
				}
			} else {
				play_animation(&t->laser_body_sprite, 0, false);
				play_animation(&t->laser_body_sprite, 2, false);
			}
		}
	} break;

	case BossTerminalsState_Laser: {
		DShape pc;
        pc.type       = DShapeType_Rect;
        pc.r.position = vec2(t->x_position, 0);
        pc.r.size     = vec2(32, 500);

        if (collision(pc, gs->ship.collider) && t->damage_again_timer > 0.5) {
	        if (damage_ship(t->laser_damage)) {
	            t->damage_again_timer = 0;
	        }
			
	    }
		if(collision(pc, gs->ship.field_collider)){
			if(t->delay <= 0){
				t->delay = 0.15;
				//energy_gained += 10;
				gs->eflow.energy_earned_in_this_cicle += 1;
				gs->ship.energy += 1;
				for (int i = 0; i < 5; i += 1) {
					Vec2 pos = {};
					pos.x = t->x_position;
					pos.y = 125;
					pos.x += (float)GetRandomValue(-15, 15);
					pos.y += (float)GetRandomValue(-15, 15);
					create_particle(pos, vec2(0,4), vec2(3,3), (Color){154, 250, 158, 255}, 1,
						true, &gs->ship.position);
				}
			}
		}
		t->delay -= delta;
		t->timer += delta;
		if (t->timer > 3) {
			t->timer = 0;
			t->state = BossTerminalsState_Initial;
		}
	} break;
	
	case BossTerminalsState_Smashing: {
		float top_terminal_y_target    = t->smash_y_target + 12;
		float bottom_terminal_y_target = t->smash_y_target - 12;

		float amount_of_movement = 500 * delta;
		float diff = t->top_terminal_y - top_terminal_y_target;

		if (fabs(amount_of_movement) > fabs(diff)) {
			t->top_terminal_y    = top_terminal_y_target;
			t->bottom_terminal_y = bottom_terminal_y_target;
		} else {
			t->top_terminal_y    -= amount_of_movement;
			t->bottom_terminal_y += amount_of_movement;
		}

		t->timer += delta;
		if (t->timer > 3) {
			t->timer = 0;
			t->state = BossTerminalsState_AfterSmashing;
		}
	} break;

	case BossTerminalsState_AfterSmashing: {
		float top_terminal_y_target    = 240;
		float bottom_terminal_y_target = 11;

		float amount_of_movement = 100 * delta;
		float diff = top_terminal_y_target - t->top_terminal_y;

		if (fabs(amount_of_movement) > fabs(diff)) {
			t->top_terminal_y    = top_terminal_y_target;
			t->bottom_terminal_y = bottom_terminal_y_target;

			t->timer = 0;
			t->activations_qnt = 0;
			t->state = BossTerminalsState_Initial;

		} else {
			t->top_terminal_y    += amount_of_movement;
			t->bottom_terminal_y -= amount_of_movement;
		}
	} break;

	}

    t->top_terminal_collider.r.position = vec2(t->x_position, t->top_terminal_y);
    if (collision(t->top_terminal_collider, gs->ship.collider) && t->damage_again_timer > 0.5) {
        if (damage_ship(t->body_damage)) {
            t->damage_again_timer = 0;
        }
    }

    t->bottom_terminal_collider.r.position = vec2(t->x_position, t->bottom_terminal_y);
    if (collision(t->bottom_terminal_collider, gs->ship.collider) && t->damage_again_timer > 0.5) {
        if (damage_ship(t->body_damage)) {
            t->damage_again_timer = 0;
        }
    }

    t->hit_cooldown -= delta;
	t->damage_again_timer += delta;
}

internal void
render_terminal() {
	BossTerminals *t = &gs->boss.terminals;

    if (t->hit_cooldown <= 0 || (total_frame_count % 6 == 0)) {
        if (t->state == BossTerminalsState_Warning || t->state == BossTerminalsState_Laser) {
            int i = 0;
            Vec2 it_pos = {};
            it_pos.x = t->x_position;
            while (true) {
                draw_sprite(&t->laser_body_sprite, it_pos, !i);
                i += 1;
                it_pos.y += 32;
                if (it_pos.y > S_HEIGHT) break;
            }
        }

        draw_sprite(&t->top_terminal_sprite   , vec2(t->x_position, t->top_terminal_y));
        draw_sprite(&t->bottom_terminal_sprite, vec2(t->x_position, t->bottom_terminal_y));
    }
}

internal void
update_boss() {
	if (!gs->boss.enabled) return;

    if (!gs->boss.defeated) {

        Enemy *core;
        for (int i = 1; i < gs->enemies_last_id; i += 1) {
            core = &gs->enemies[i];
            if (core->id == gs->boss.boss_core_id) {
                break;
            }
        }

    	switch (gs->boss.stage) {
    	case 0: update_terminal();
        
        case 1: {
            play_animation(&core->sprite, 1, true);
            if (gs->upper_cannon_destroyed && gs->center_cannon_destroyed && gs->lower_cannon_destroyed) {
                gs->boss.stage = 2;
                core->state = EnemyState_Initial;
            }
        } break;

        case 2: {
            play_animation(&core->sprite, 2, true);
        } break;

    	} 
    } else {
        if (gs->boss.defeated_timer == 0) {
            create_random_particles(500, vec2(375,125), vec2(80,50), vec2(40,40), WHITE, LIGHTGRAY, true, vec2(5,5));
        }
        gs->boss.defeated_timer += delta;
        if (gs->boss.defeated_timer > 5) {
            gs->boss.variable_to_check_if_boss_is_death = true;
            gs->boss.enabled = false;
            gs->level = 3;
        }
    }
}

internal void
render_boss() {
	if (!gs->boss.enabled) return;

    DrawTextureEx(gs->boss_heart_base_texture, {750, 100}, 0, 2, WHITE);

	switch (gs->boss.stage) {
	case 0: {
        DrawTextureEx(gs->boss_shield_texture, {700, 100}, 0, 2, WHITE);
        render_terminal();
    } break;
	}
}
