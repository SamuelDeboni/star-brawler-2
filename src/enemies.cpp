internal u16
spawn_boss_core() {
	Enemy e  = {};
	e.speed  = 40;
	e.type   = EnemyType_Boss_Core;
	e.sprite = gs->boss_core_sprite;
	e.position = vec2(450,125);
	e.collider.type     = DShapeType_Circle;
   	e.collider.c.radius = 80;
	e.damage = 40;
	e.hp = 40;
	e.timer = 0;
	e.state = EnemyState_Protected;
	play_animation(&e.sprite, 0, true);
	return add_enemy(e);
}

internal void
spawn_upper_cannon(Vec2 pos, float boost) {
	Enemy e  = {};
	e.speed  = 40 * boost;
	e.type   = EnemyType_Upper_Cannon;
	e.sprite = gs->upper_cannon_sprite;
	e.position = pos;
	e.collider.type = DShapeType_Rect;
	e.collider.r.size = vec2(64, 32);
	e.damage = 40;
	e.hp = 40;
	e.timer = 2;
	e.laser_sprite = gs->great_plasma_laser_sprite;
	play_animation(&e.sprite, 0, false);
	play_animation(&e.laser_sprite, 0, true);
	add_enemy(e);
}

internal void
spawn_center_cannon(Vec2 pos, float boost) {
	Enemy e  = {};
	e.speed  = 40 * boost;
	e.type   = EnemyType_Center_Cannon;
	e.sprite = gs->center_cannon_sprite;
	e.position = pos;
	e.collider.type = DShapeType_Rect;
	e.collider.r.size = vec2(64, 32);
	e.damage = 40;
	e.hp = 40;
	e.timer = 2;
	e.laser_sprite = gs->great_plasma_laser_sprite;
	play_animation(&e.sprite, 0, false);
	play_animation(&e.laser_sprite, 0, true);
	add_enemy(e);
}

internal void
spawn_lower_cannon(Vec2 pos, float boost) {
	Enemy e  = {};
	e.speed  = 40 * boost;
	e.type   = EnemyType_Lower_Cannon;
	e.sprite = gs->lower_cannon_sprite;
	e.position = pos;
	e.collider.type = DShapeType_Rect;
	e.collider.r.size = vec2(64, 32);
	e.damage = 40;
	e.hp = 40;
	e.timer = 2;
	e.laser_sprite = gs->great_plasma_laser_sprite;
	play_animation(&e.sprite, 0, false);
	play_animation(&e.laser_sprite, 0, true);
	add_enemy(e);
}

internal void
spawn_boomerang(Vec2 pos, float boost) {
	Enemy e  = {};
	e.speed  = 40 * boost;
	e.type   = EnemyType_Boomerang;
	e.sprite = gs->boomerang_sprite;
	e.position = pos;
	e.collider.type = DShapeType_Rect;
	e.collider.r.size = vec2(32, 32);
	e.damage = 40;
	e.hp = 2;
	play_animation(&e.sprite, 0, true);
	add_enemy(e);
}

internal void
spawn_laser_bit(Vec2 pos, float boost) {
	Enemy e  = {};
	e.speed  = 40 * boost;
	e.type   = EnemyType_LaserBit;
	e.sprite = gs->laser_bit_sprite;
	e.position = pos;
	e.collider.type = DShapeType_Rect;
	e.collider.r.size = vec2(16, 32);
	e.damage = 40;
	e.hp = 5;
	e.laser_sprite = gs->plasma_laser_sprite;
	play_animation(&e.sprite, 0, true);
	play_animation(&e.laser_sprite, 2, true);
	add_enemy(e);	
}

internal void
spawn_basic_ship(Vec2 pos, float boost) {
	Enemy e  = {};
	e.speed  = 40 * boost;
	e.type   = EnemyType_BasicShip;
	e.sprite = gs->auric_ship_sprite;
	e.position = pos;
	e.collider.type = DShapeType_Rect;
	e.collider.r.size = vec2(26, 31);
	e.damage = 40;
	e.hp = 5;
	play_animation(&e.sprite, 0, true);
	add_enemy(e);	
}

internal void
spawn_debrie(Vec2 pos) {
	Enemy e     = {};
	e.type      = EnemyType_Debris;
	e.sprite    = gs->debrie_sprite;
	e.position  = pos;
	e.velocity = vec2(-200, 0);

	e.collider.type   = DShapeType_Rect;
	e.collider.r.size = vec2(32, 32);
	e.damage = 40;
	e.hp     = 2;

	play_animation(&e.sprite, 0, true);
	add_enemy(e);	
}

internal u16
spawn_cluster(Vec2 pos, float boost) {
	Enemy e  = {};
	e.speed  = 40 * boost;
	e.type   = EnemyType_Cluster;
	e.sprite = gs->cluster_sprite;
	e.position = pos;
	e.collider.type = DShapeType_Rect;
	e.collider.r.size = vec2(16, 32);
	e.damage = 40;
	e.hp = 8;
	play_animation(&e.sprite, 0, true);
	return add_enemy(e);
}

internal void
spawn_static_seeker(Vec2 pos, float boost) {
	Enemy e  = {};
	e.speed  = 40 * boost;
	e.type   = EnemyType_Seeker;
	e.sprite = gs->seeker_sprite;
	e.position = pos;
	e.collider.type = DShapeType_Rect;
	e.collider.r.size = vec2(10, 10);
	e.damage = 40;
	e.hp = 1;
	play_animation(&e.sprite, 0, true);
	add_enemy(e);
}

internal void
spawn_setting_seeker(Vec2 pos, Vec2 direction, float boost) {
	Enemy e  = {};
	e.speed  = 40 * boost;
	e.type   = EnemyType_Seeker;
	e.sprite = gs->seeker_sprite;
	e.direction = direction;
	e.position  = pos;
	e.collider.type = DShapeType_Rect;
	e.collider.r.size = vec2(10, 10);
	e.damage = 40;
	e.state = EnemyState_Setting;
	e.hp = 1;
	play_animation(&e.sprite, 2, false);
	add_enemy(e);
}

internal void
spawn_positioning_seeker(Vec2 pos, Vec2 target, float boost) {
	Enemy e  = {};
	e.speed  = 40 * boost;
	e.type   = EnemyType_Seeker;
	e.sprite = gs->seeker_sprite;
	e.target = target;
	e.position  = pos;
	e.collider.type = DShapeType_Rect;
	e.collider.r.size = vec2(10, 10);
	e.damage = 40;
	e.state = EnemyState_Positioning;
	e.hp = 1;
	play_animation(&e.sprite, 2, false);
	add_enemy(e);
}



internal void
enemy_basic_shoot(Vec2 pos, Vec2 direction) {
	Projectile p = {};
	p.position   = pos;
	p.direction  = direction;
	p.sprite     = gs->plasma_sphere_sprite;
	p.collider.type = DShapeType_Rect;
	p.collider.r.size = vec2(10, 10);
	p.speed      = 120;
	p.damage     = 20;
	p.enemy      = true;
	play_animation(&p.sprite, 0, true);
	add_projectile(p);
}

internal void
update_boss_core(Enemy *it) {
	switch (it->state) {
	case EnemyState_Initial: {
		it->timer += delta;
		if (it->timer > 2) { 
			for (int i = 0; i < 15; i += 1) {
				double theta = (double)(i * 8 + 120+GetRandomValue(0,15)) * (DM_PI / 180.0);

				enemy_basic_shoot(it->position + vec2(80,0), (1+(float)GetRandomValue(0,5)/10)*(Vec2){ float(cos(theta)), float(sin(theta)) });
			}
			it->timer = 1.8-(float)GetRandomValue(0,18)/10;
		}
	} break;
	}
}

internal void
update_cannon(Enemy *it) {
	switch (it->state) {
	case EnemyState_Initial: {
		if(it->timer <= 0){
			int pattern = GetRandomValue(0, 7);
			if(pattern < 2){
				for(int i=0;i<4;i++){
					enemy_basic_shoot(it->position + vec2(-12, 0), normalize(vec2(-1, -1.5+i)));
				}
				it->timer = 1+ (float)GetRandomValue(0, 10)/10;
			}
			else if(pattern < 4){
				for(int i=0;i<5;i++){
					enemy_basic_shoot(it->position + vec2(-12, 0), normalize(vec2(-1, -2+i)));
				}
				it->timer = 1 + (float)GetRandomValue(0, 10)/10;
			}else if(pattern < 6){
				enemy_basic_shoot(it->position + vec2(-12, 16), vec2(-1, 0));
				enemy_basic_shoot(it->position + vec2(-28, 0), vec2(-1, 0));
				enemy_basic_shoot(it->position + vec2(-12, -16), vec2(-1, 0));

				it->timer = 1 + (float)GetRandomValue(0, 10)/10;
			}
			else{
				it->state = EnemyState_Aiming;
				it->timer = 2;
				play_animation(&it->laser_sprite, 0, true);
			}
		}
		it->timer -= delta;
	} break;
	case EnemyState_Aiming: {
		if(it->timer <= 0){
			it->state = EnemyState_Shooting;
			it->timer = 3;
			play_animation(&it->laser_sprite, 1, true);
		}
		it->timer -= delta;
	} break;
	case EnemyState_Shooting: {
		DShape pc;
    	pc.type = DShapeType_Rect;
		pc.r.position = vec2(it->position.x/2, it->position.y);
	    pc.r.size     = vec2(it->position.x, 26);
		if (collision(pc, gs->ship.collider)) {
    	    damage_ship(40);
    	}
		if(collision(gs->ship.field_collider, pc)/* && gs->ship.field_sprite.animation == 0*/){
			if(it->delay <= 0){
				it->delay = 0.15;
				//energy_gained += 10;
				gs->eflow.energy_earned_in_this_cicle += 1;
				gs->ship.energy += 1;
				for (int i = 0; i < 5; i += 1) {
					Vec2 pos = it->position;
					pos.x += (float)GetRandomValue(-15, 15);
					pos.y += (float)GetRandomValue(-15, 15);
					create_particle(pos, vec2(0,4), vec2(3,3), (Color){154, 250, 158, 255}, 1,
						true, &gs->ship.position);
				}
			}
		}
		if(it->timer <= 0){
			it->state = EnemyState_Initial;
			it->timer = 2;
		}
		it->delay -= delta;
		it->timer -= delta;
	} break;
	/*case EnemyState_Recovering: {
		if(it->timer <= 0){
			it->state = EnemyState_Initial;
		}
		it->timer -= delta;
	} break;*/
	}
}

internal void
update_boomerang(Enemy *it) {
	switch (it->state) {

	case EnemyState_Setting: {
		it->velocity  = normalize(it->direction) * it->speed;
		it->position += it->velocity * delta;

		it->timer += delta;
		if (it->timer > 3) {
			it->timer = 0;
			it->state = EnemyState_Initial;
		}
	} break;

	case EnemyState_Initial: {
		Vec2 direction = gs->ship.position - it->position;
		it->velocity   = normalize(direction) * it->speed;
		f32 dist       = magnitude(direction);
		it->position  += it->velocity * delta;

		if (dist < SEEKER_RUSH_THRESHOLD) {
			it->target = gs->ship.position + vec2(10, 0);
			it->state  = EnemyState_Rush;
			it->velocity = normalize(gs->ship.position - it->position) * 200;

			bool positive    = GetRandomValue(0, 1);
			int  euler_angle = (GetRandomValue(0, 45) + 10) * (positive ? 1 : -1);
			double theta  = (double)(euler_angle) * (DM_PI / 180.0);

			it->velocity.x = it->velocity.x * cos(theta) + it->velocity.y * -sin(theta);
			it->velocity.y = it->velocity.x * sin(theta) + it->velocity.y * cos(theta);

			it->timer = 0;

			create_random_particles(25, it->position, vec2(10,10), vec2(5,5), WHITE, PURPLE);
		}
	} break;

	case EnemyState_Rush: {
		it->position  += it->velocity * delta;
		it->timer += delta;
		if (it->timer > 1.5) {
			it->timer = 0;
			it->state = EnemyState_GoingBack;
		}

		create_random_particles(1, it->position, vec2(5,5), vec2(5,5), WHITE, PURPLE);
	} break;

	case EnemyState_GoingBack: {
		Vec2 direction    = gs->ship.position - it->position;
		Vec2 expected_vel = normalize(direction) * 200;
		it->velocity      = lerp(it->velocity, expected_vel, delta * 3);
		it->position     += it->velocity * delta;

		it->timer += delta;
		if (it->timer > 0.7) {
			it->velocity = normalize(gs->ship.position - it->position) * 200;
			it->state = EnemyState_RushingAgain;

			float vel_angle = atan(it->velocity.y / it->velocity.x);
			
			double theta;

			theta = 45.0 * (DM_PI / 180.0) + vel_angle;
			spawn_setting_seeker(it->position, (Vec2){ float(cos(theta)), float(sin(theta)) }, 1.0);
		    
			theta = -45.0 * (DM_PI / 180.0) + vel_angle;
		    spawn_setting_seeker(it->position, (Vec2){ float(cos(theta)), float(sin(theta)) }, 1.0);
		}
	} break;

	case EnemyState_RushingAgain: {
		it->position  += it->velocity * delta;

		create_random_particles(1, it->position, vec2(5,5), vec2(5,5), WHITE, PURPLE);
	} break;

	case EnemyState_Positioning: {
		Vec2 direction = it->target - it->position;
		it->velocity = normalize(direction) * it->speed;
		f32 dist = magnitude(direction);

		if (dist < 5) {
			//it->timer += delta;
			//if (it->timer > 1.0) {
			it->state = EnemyState_Initial;
			//}
		} else {
			it->position += it->velocity * delta;
		}
	} break;

	}
}


internal void
update_laser_bit(Enemy *it) {
	it->position.x -= delta * 70;

	switch (it->state) {
	case EnemyState_Initial: {
		it->timer += delta;

		if (it->timer > 0.5) {
			if (it->laser_sprite.animation % 2 == 0) {
				play_animation(&it->laser_sprite, it->laser_sprite.animation + 1, true);
			}

			DShape pc;
    	    pc.type = DShapeType_Rect;

			if (it->sprite.animation == 0) {
   	 	    	pc.r.position = vec2(it->position.x, 0);
	    	    pc.r.size     = vec2(4, 300);
			} else {
   	 	    	pc.r.position = vec2(0, it->position.y);
	    	    pc.r.size     = vec2(450, 4);
    		}

    		if (collision(pc, gs->ship.collider)) {
    	    	damage_ship(20);
    	    }
			if(collision(gs->ship.field_collider, pc) && gs->ship.field_sprite.animation == 0){
				if(it->delay <= 0){
					it->delay = 0.15;
					//energy_gained += 10;
					gs->eflow.energy_earned_in_this_cicle += 1;
					gs->ship.energy += 1;
					for (int i = 0; i < 5; i += 1) {
						Vec2 pos = it->position;
						pos.x += (float)GetRandomValue(-15, 15);
						pos.y += (float)GetRandomValue(-15, 15);
						create_particle(pos, vec2(0,4), vec2(3,3), (Color){154, 250, 158, 255}, 1,
							true, &gs->ship.position);
					}
				}
			}
		}

		if (it->timer > 1) {
			it->state = EnemyState_Aiming;
			play_animation(&it->sprite, it->sprite.animation == 0 ? 1 : 3, false);
		}

		if (total_frame_count & 1) {
			create_random_particles(1, it->position, vec2(5,5), vec2(5,5), WHITE, PURPLE);
		}
		it->delay -= delta;
	} break;
	case EnemyState_Aiming: {
		bool equals_to_one = it->sprite.animation == 1;

		if (equals_to_one) {
			it->collider.r.size = it->sprite.frame < 3 ? vec2(16, 32) : vec2(32, 16);
		} else {
			it->collider.r.size = it->sprite.frame < 3 ? vec2(32, 16) : vec2(16, 32);			
		}

		if (it->sprite.has_finished) {
			it->timer = 0;
			it->state = EnemyState_Initial;
			play_animation(&it->sprite,       equals_to_one ? 2 : 0, false);
			play_animation(&it->laser_sprite, equals_to_one ? 0 : 2, true);

			it->collider.r.size = equals_to_one ? vec2(32, 16) : vec2(16, 32);
		}
	} break;
	}
}


internal void
update_basic_ship(Enemy *it) {
	switch (it->state) {
	case EnemyState_Initial: {
		bool can_change_states = true;
		if (gs->ship.position.x < it->position.x) {
			Vec2 direction  = gs->ship.position + vec2(10, 0) - it->position;
			it->velocity    = normalize(direction) * it->speed;
			f32 dist        = magnitude(direction);
			it->position   += it->velocity * delta;
		} else {
			it->position   += vec2(-1, 0) * it->speed * 2 * delta;
			can_change_states = false;
		}

		it->timer += delta;
		if (it->timer > 3 && can_change_states) {
			it->velocity = {};
			it->timer = 0;
			it->projectiles_shooted = 0;
			it->state = EnemyState_Aiming;
			it->delay = (float)GetRandomValue(0, 300)/100;
		}

		if (total_frame_count & 1) {
			create_random_particles(1, it->position, vec2(5,5), vec2(5,5), WHITE, PURPLE);
		}
	} break;
	case EnemyState_Aiming: {
		if (gs->ship.position.x < it->position.x) {
			float diff = it->position.y - gs->ship.position.y;
			if (diff < 30 && diff > -30) {
				it->velocity.y = lerp(it->velocity.y, 0, delta * 3);
			} else {
				bool positive_movement = it->position.y < gs->ship.position.y;
				it->velocity.y = it->speed * 2 * (positive_movement ? 1 : -1);
			}
			it->position.y += it->velocity.y * delta;
		}

		it->timer += delta;
		if (it->timer > it->delay) {
			it->timer = 0;
			it->delay = (float)GetRandomValue(0, 300)/100;
			it->state = gs->ship.position.x < it->position.x ? EnemyState_Shooting : EnemyState_Initial;
		}
	} break;
	case EnemyState_Shooting: {
		it->timer += delta;
		if (it->timer > 0.3) {
			enemy_basic_shoot(it->position + vec2(-16, 0), vec2(-1, 0));
	    	it->timer = 0;

	    	it->projectiles_shooted += 1;
	    	it->state = it->projectiles_shooted < 3 ? EnemyState_Aiming : EnemyState_Initial;
	    	if(it->state == EnemyState_Aiming){
	    		it->delay = (float)GetRandomValue(0, 300)/100;
	    	}
	    }		    	
	} break;
	}
}


internal void
update_seeker(Enemy *it) {
	switch (it->state) {

	case EnemyState_Setting: {
		it->velocity  = normalize(it->direction) * it->speed;
		it->position += it->velocity * delta;

		it->timer += delta;
		if (it->timer > 3) {
			it->timer = 0;
			it->state = EnemyState_Initial;
			play_animation(&it->sprite, 0, true);
		}
	} break;

	case EnemyState_Initial: {
		Vec2 direction = gs->ship.position - it->position;
		it->velocity   = normalize(direction) * it->speed;
		f32 dist       = magnitude(direction);
		it->position  += it->velocity * delta;

		if (dist < SEEKER_RUSH_THRESHOLD) {
			it->target = gs->ship.position + vec2(10, 0);
			it->state  = EnemyState_Rush;
			it->velocity = normalize(gs->ship.position - it->position) * 200;
			play_animation(&it->sprite, 1, true);

			create_random_particles(25, it->position, vec2(10,10), vec2(5,5), WHITE, PURPLE);
		}
	} break;

	case EnemyState_Rush: {
		it->position  += it->velocity * delta;

		create_random_particles(1, it->position, vec2(5,5), vec2(5,5), WHITE, PURPLE);
	} break;

	case EnemyState_Positioning: {
		Vec2 direction = it->target - it->position;
		it->velocity = normalize(direction) * it->speed;
		f32 dist = magnitude(direction);

		if (dist < 5) {
			//it->timer += delta;
			//if (it->timer > 1.0) {
			it->state = EnemyState_Initial;
			//}
		} else {
			it->position += it->velocity * delta;
		}
	} break;

	}
}

internal void
update_cluster(Enemy *it) {
	switch (it->state) {
	case EnemyState_Initial: {
		if (!it->is_direction_setted) {
			it->direction = normalize(gs->ship.position - it->position);
			it->speed = it->speed * ((float)GetRandomValue(250, 1000) / 500.0f);
			it->is_direction_setted = true;
		}

		it->velocity  = it->direction * it->speed;
		it->position += it->velocity * delta;

		it->timer += delta;
		if (it->timer > 5) {
			for (int i = 0; i < 10; i += 1) {
				double theta = (double)(i * 36) * (DM_PI / 180.0);

				enemy_basic_shoot(it->position, (Vec2){ float(cos(theta)), float(sin(theta)) });
			}

			for (int i = 0; i < 5; i += 1) {
				double theta = (double)GetRandomValue(0, 359) * (DM_PI / 180.0);
		    	spawn_setting_seeker(it->position, (Vec2){ float(cos(theta)), float(sin(theta)) }, 1.0);
			}

	        PlaySound(gs->cluster_explosion_sound);
			gs->enemies_delete_queue.push(it->id);
		}
	} break;
	}
}

internal void
update_mini_boss(MiniBoss *it) {
	if (it->disabled) return;

	it->collider.r.position = it->position;
	it->damage_cooldown += delta;

	switch (it->state) {
	case MiniBossState_Initial: {
	    play_animation(&it->sprite, 0, false);
	    if (it->sprite.has_finished) {
	    	it->state = MiniBossState_Charging;
	    	play_animation(&it->sprite, 1, false);
	    }
	} break;

	case MiniBossState_Charging: {
	    if (it->sprite.has_finished) {
			it->velocity = it->vel_multiplier * 200 * normalize(gs->ship.position - it->position);
			it->state = MiniBossState_Dash;
			it->timer = 0;

			if (it->vel_multiplier < 3) {
				it->vel_multiplier += 0.25;
			}

	    	play_animation(&it->sprite, 2);

	        create_random_particles(25, gs->mini_boss.position, vec2(25, 25), vec2(50, 50), WHITE, PURPLE);
		}
	} break;

	case MiniBossState_Dash: {
		if (it->position.x < 25 || it->position.x > 425) {
			it->velocity.x = -it->velocity.x;
		}

		if (it->position.y < 25 || it->position.y > 225) {
			it->velocity.y = -it->velocity.y;
		}

        create_random_particles(2, gs->mini_boss.position, vec2(25, 25), vec2(0, 0), WHITE, PURPLE);

		Vec2 new_pos = it->position;
		new_pos += it->velocity * delta;

		bool collide_with_border = false;
		if (new_pos.x < 25 || new_pos.x > 425) {
			it->velocity.x = -it->velocity.x;
			collide_with_border = true;
		}
		if (new_pos.y < 25 || new_pos.y > 225) {
			it->velocity.y = -it->velocity.y;
			collide_with_border = true;
		}

		if (!collide_with_border) {
			it->position += it->velocity * delta;
		}

		if (magnitude(it->velocity) > 100) {
			it->velocity -= it->velocity * delta * 0.1;
		}

		it->timer += delta;
		if (it->timer >= 10.0 && it->timer < 20) {
			it->timer = 21;

			Vec2 offsets[4] = {
				{-25, -25},
				{-25,  25},
				{ 25, -25},
				{ 25,  25}
			};
			for (int i = 0; i < it->bomb_count; i++) {
				u16 id = spawn_cluster(it->position + offsets[i], 1);
				Enemy *cluster = &gs->enemies[id];
				Vec2 target = vec2(225 + GetRandomValue(-25, 25), 150 + GetRandomValue(-25, 25));
				cluster->direction = normalize(target - cluster->position);
				cluster->is_direction_setted = true;
			}

			if (it->bomb_count < 4) it->bomb_count++;
		}

		if (it->timer >= 15.0) {
			it->timer = 0;
			it->state = MiniBossState_Charging;
	    	play_animation(&it->sprite, 1, false);
		}
	} break;

	}

	if (collision(it->collider, gs->ship.collider) &&
		it->state != MiniBossState_Initial)
	{
		damage_ship(40);
	}
}

internal void
draw_mini_boss(MiniBoss *it) {
	if (it->disabled) return;

	if (it->damage_cooldown >= 0.15) {
		draw_sprite(&it->sprite, it->position);
	} else {
		draw_sprite(&it->sprite, it->position, true, total_frame_count % 6 == 0);
	}
}


internal void
update_debries(Enemy *it) {
	it->position += it->velocity * delta;

	if (it->position.x < gs->explosion_x) {
		if (gs->explosion_beam_c < 32) {
			gs->explosion_beam_y[gs->explosion_beam_c] = it->position.y;
			gs->explosion_beam_t[gs->explosion_beam_c] = 7;
			gs->explosion_beam_c++;
		}

		gs->enemies_delete_queue.push(it->id);
	}
}
