#define ARRAY_SIZE(A) (sizeof(A)/sizeof(A[0]))

template <typename T, size_t S>
struct StaticList {
    size_t count = 0;
    T data[S];

    bool push(T value) {
        if (count >= S) return false;
        data[count++] = value;

        return true;
    }

    T pop() {
        T result = {};

        if (count > 0) {
            count--;
            result = data[count];
        }

        return result;
    }

    bool insert(T value, int pos) {
        if (count >= S) return false;

        for (int i = count; i > pos; i--) {
            data[i] = data[i - 1];
        }

        count++;
        data[pos] = value;

        return true;
    }

    T remove(int pos) {
        T result = {};
        if (count > 0) {
            result = data[pos];
            count--;

            for (int i = pos; i < count; i++) {
                data[i] = data[i + 1];
            }
        }

        return result;
    }
};

internal Vector2
to_ray_vec(Vec2 in) {
    return (Vector2){ in.x * S_SCALE, S_HEIGHT - in.y * S_SCALE };
}

internal void
draw_rect(Vec2 pos, Vec2 size, Color color) {
    DrawRectangle(
        (pos.x - size.x * 0.5f) * S_SCALE,
        S_HEIGHT - ((pos.y + size.y * 0.5f) * S_SCALE),
        size.x * S_SCALE, size.y * S_SCALE, color
    );
}