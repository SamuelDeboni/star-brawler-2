#define MAX_PARTICLES 4096

struct ParticleSystem {
	Vec2  pos[MAX_PARTICLES];
	Vec2  vel[MAX_PARTICLES];
	Vec2  size[MAX_PARTICLES];
	f32   time[MAX_PARTICLES];
	Color color[MAX_PARTICLES];
	bool  square[MAX_PARTICLES];
	Vec2 *target[MAX_PARTICLES];

	int count;	

	Texture texture;
};

global ParticleSystem *ps;

internal void
create_particle(Vec2 position, Vec2 velocity, Vec2 size, Color color, f32 life_time,
                bool square = false, Vec2 *target = NULL) {
	if (ps->count >= MAX_PARTICLES) return;

	ps->pos[ps->count]    = position;
	ps->vel[ps->count]    = velocity;
	ps->size[ps->count]   = size;
	ps->color[ps->count]  = color;
	ps->time[ps->count]   = life_time;
	ps->square[ps->count] = square;
	ps->target[ps->count] = target;
	ps->count++;
}

internal void
create_random_particles(int qnt, Vec2 _pos, Vec2 pos_variation, Vec2 vel_variation, Color c1, Color c2,
                        bool square = false, Vec2 size = (Vec2){4, 4}, float duration = 0.5) {
	for (int idx = 0; idx < qnt; idx++) {
		Color color = GetRandomValue(0, 1) ? c1 : c2;

		Vec2 pos = _pos;
		pos.x += (float)GetRandomValue(-pos_variation.x, pos_variation.x);
		pos.y += (float)GetRandomValue(-pos_variation.y, pos_variation.y);

		Vec2 vel;
		vel.x = (float)GetRandomValue(-vel_variation.x, vel_variation.x);
		vel.y = (float)GetRandomValue(-vel_variation.y, vel_variation.y);

		create_particle(pos, vel, size, color, duration, square);
	}
}

internal void
draw_particles() {

	// Update
	for (int i = 0; i < ps->count; i++) {
		if (ps->target[i] == NULL) {
			ps->pos[i] += ps->vel[i]  * delta;
			ps->size[i] -= ps->size[i] * delta * 2;
		} else {
			ps->pos[i]  = lerp(ps->pos[i], *ps->target[i], delta * ps->vel[i].y);
			ps->size[i] -= ps->size[i] * delta;
		}
		ps->time[i] -= delta;
	}

	// Draw
	for (int i = 0; i < ps->count; i++) {
		if (!ps->square[i]) {
			DrawTextureEx(ps->texture,
 	    		to_ray_vec(ps->pos[i] - vec2(ps->size[i].x, -ps->size[i].y * 0.5f)),
 	    		0, ps->size[i].x * 0.5, ps->color[i]);
		} else {
			draw_rect(ps->pos[i], ps->size[i], ps->color[i]);
 	    }
	}

	// Destroy
	for (int i = 0; i < ps->count; i++) {
		if (ps->time[i] <= 0) {
			ps->count--;

			ps->pos   [i] = ps->pos[ps->count];
			ps->vel   [i] = ps->vel[ps->count];
			ps->size  [i] = ps->size[ps->count];
			ps->time  [i] = ps->time[ps->count];
			ps->color [i] = ps->color[ps->count];
			ps->square[i] = ps->square[ps->count];
			ps->target[i] = ps->target[ps->count];
		}
	}
}
