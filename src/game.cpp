#define PROJECTILE_TTL_SECS 5.0f

#define SEEKER_RUSH_THRESHOLD          150
#define SEEKER_RUSH_BOOST              5
#define SEEKER_RECOVER_THRESHOLD       5
#define SEEKER_RECOVER_TIME            3.5
#define SEEKER_RECOVER_SLOWDOWN_FACTOR 3
#define FLARE_COST 30
#define LASER_COST 50
#define BFL_COST 100


enum ShipPowerup {
    ShipPowerup_Laser,
    ShipPowerup_Flare,
    ShipPowerup_BFL,
};

struct Ship {
    Vec2   position;
    DShape collider;

    DShape field_collider;

    float  punch_timer;
    Sprite sprite;
    Sprite field_sprite;
    float  energy;
    float  damage_cooldown;
    float  field_cooldown;
    Sprite laser_pointer_sprite;
    Sprite laser_body_sprite;
    Sprite bfl_pointer_sprite;
    Sprite bfl_body_sprite;
    Sprite flare_sprite;
    float  powerup_active_cooldown;
    ShipPowerup actual_powerup;

    int lives;
};

enum EnemyType {
    EnemyType_Seeker,
    EnemyType_BasicShip,
    EnemyType_Cluster,
    EnemyType_LaserBit,
    EnemyType_Boomerang,
    EnemyType_Debris,
    EnemyType_Upper_Cannon,
    EnemyType_Center_Cannon,
    EnemyType_Lower_Cannon,
    EnemyType_Boss_Core,
};

struct Projectile {
    u16     id;
    Vec2    position;
    Vec2    direction;
    Sprite  sprite;
    float   speed;
    float   timer;
    int     damage;
    bool    enemy;
    DShape  collider;
};

enum EnemyState {
    EnemyState_Initial = 0,
    EnemyState_Rush,
    EnemyState_Recovering,
    EnemyState_Aiming,
    EnemyState_Shooting,
    EnemyState_Setting,
    EnemyState_Positioning,
    EnemyState_GoingBack,
    EnemyState_RushingAgain,
    EnemyState_Protected,
};

struct Enemy {
    u16        id;
    Sprite     sprite;
    Vec2       position;
    Vec2       velocity;
    Vec2       target;
    Vec2       direction;
    int        projectiles_shooted;
    EnemyType  type;
    float      speed;
    EnemyState state;
    float      timer;
    int        damage;
    float      damage_again_timer;
    bool       is_direction_setted;
    float      hp;
    float      hit_cooldown;
    float      delay;

    Sprite     laser_sprite;

    float     *hp_ref;

    DShape     collider;
};

enum MiniBossState {
    MiniBossState_Initial,
    MiniBossState_Charging,
    MiniBossState_Dash,
};

struct MiniBoss {
    MiniBossState state;
    Vec2   position;
    Vec2   velocity;
    Sprite sprite;
    b32    disabled;
    DShape collider;
    f32    timer;
    f32    vel_multiplier;
    f32    hp;
    f32    damage_cooldown;
    int    bomb_count;
};

enum BossTerminalsState {
	BossTerminalsState_Initial,
	BossTerminalsState_Warning,
	BossTerminalsState_Laser,
    BossTerminalsState_Smashing,
    BossTerminalsState_AfterSmashing,
};

struct BossTerminals {
	float  x_position;
	DShape top_terminal_collider;
	Sprite top_terminal_sprite;
    float  top_terminal_y;
	DShape bottom_terminal_collider;
	Sprite bottom_terminal_sprite;
    float  bottom_terminal_y;
    float  smash_y_target;
	f32    hp;
	Sprite laser_body_sprite;
	DShape laser_collider;
	float  body_damage;
	float  laser_damage;
    int    laser_warning_blinks;
    float  timer;
    float  damage_again_timer;
	int    activations_qnt;
    float  hit_cooldown;

    float delay;
	BossTerminalsState state;
};

struct Boss {
	BossTerminals terminals;

    int    boss_core_id;

	int   stage;
	bool  enabled;
    bool  defeated;
    float defeated_timer;

    bool variable_to_check_if_boss_is_death;
};

#define EFLOW_ENTRIES   29
#define MAX_ENEMIES     128
#define MAX_PROJECTILES 1024

struct Eflow {
    int positive_values[EFLOW_ENTRIES];
    int negative_values[EFLOW_ENTRIES];

    float energy_in_last_cicle;
    float energy_earned_in_this_cicle;
    float cicle_cooldown;
};

struct GameState {
    Ship ship;

    Eflow eflow;

    Enemy      enemies[MAX_ENEMIES];
    int        enemies_qnt;
    int        enemies_last_id;

    StaticList<u16, MAX_ENEMIES> enemies_delete_queue;

    Projectile projectiles[MAX_PROJECTILES];
    int        projectiles_qnt;
    int        projectiles_last_id;

    StaticList<u16, MAX_PROJECTILES> projectiles_delete_queue;

    Texture2D ship_texture;
    Texture2D basic_auric_ship_texture;

    Sprite test_sprite;
    Sprite seeker_sprite;
    Sprite auric_ship_sprite;
    Sprite plasma_sphere_sprite;
    Sprite cluster_sprite;
    Sprite debrie_sprite;
    Sprite laser_sprite;
    Sprite bfl_sprite;
    Sprite plasma_laser_sprite;
    Sprite laser_bit_sprite;
    Sprite boomerang_sprite;
    Sprite great_plasma_laser_sprite;
    Sprite terminals_sprite;
    Sprite upper_cannon_sprite;
    Sprite center_cannon_sprite;
    Sprite lower_cannon_sprite;
    Sprite boss_core_sprite;

    Texture2D test_doomguy;
    Texture2D bg0_texture;
    Texture2D bg1_texture;
    Texture2D bg2_texture;
    Texture2D bg3_texture;
    Texture2D bg4_texture;
    Texture2D potato_texture;
    Texture2D explosion_texture;
    Texture2D mugshot_texture;
    Texture2D boss_heart_base_texture;
    Texture2D boss_shield_texture;
    b32 change_background;

    int mugshot;

    Font hud_font;
    int  hud_font_size;
    Texture2D hud;

    Font hud_detail_font;
    int  hud_detail_font_size;

    float bg0_scroll_x;
    float bg4_scroll_x;
    float bg3_scroll_x;

    int level;
    b32 paused;

    int wave;
    float wave_timer;

    Music current_music;
    Sound punch_sound_0;
    Sound flare_sound;
    Sound hurt_sound;
    Sound laser_sound;
    Sound bfl_sound;
    Sound cluster_explosion_sound;

    MiniBoss mini_boss;

    Boss boss; 

    bool debug_mode;

    float explosion_x;
    float explosion_beam_y[32];
    float explosion_beam_t[32];
    Sprite explosion_beam_s[32];
    int   explosion_beam_c;
    float debris_timer;

    bool upper_cannon_destroyed;
    bool center_cannon_destroyed;
    bool lower_cannon_destroyed;

    float reset_timer;
    b32   reset;

    int dificulty;
};

global GameState *gs;

internal void
game_start(bool load_resources = true) {

    if (load_resources) {
        gs = (GameState *)MemAlloc(sizeof(GameState));
        ps = (ParticleSystem *)MemAlloc(sizeof(ParticleSystem));
        ps->texture = LoadTexture("assets/Flame_Particle.png");



        // Load Textures
        gs->ship_texture             = LoadTexture("assets/RetronianShip.png");
        gs->basic_auric_ship_texture = LoadTexture("assets/AuricShip.png");
        gs->bg0_texture              = LoadTexture("assets/Background_Near_1.png");
        gs->bg2_texture              = LoadTexture("assets/Background_Far_1.png");
        gs->bg3_texture              = LoadTexture("assets/Background_Near_2.png");
        gs->bg4_texture              = LoadTexture("assets/Background_Middle_2.png");
        gs->test_doomguy             = LoadTexture("assets/Mugshot.png");
        gs->hud                      = LoadTexture("assets/hud.png");
        gs->potato_texture           = LoadTexture("assets/potato.png");
        gs->explosion_texture        = LoadTexture("assets/Explosion.png");
        gs->mugshot_texture          = LoadTexture("assets/Mugshot_Sheet.png");
        gs->boss_heart_base_texture  = LoadTexture("assets/Heart_Base.png");
        gs->boss_shield_texture      = LoadTexture("assets/Shield.png");

        // Load Sprites
        gs->test_sprite.texture          = LoadTexture("assets/AuricShip_TestSheet.png");
        gs->seeker_sprite.texture        = LoadTexture("assets/Auric_Seeker_Sheet.png");
        gs->auric_ship_sprite.texture    = LoadTexture("assets/Auric_Ship_Sheet.png");
        gs->plasma_sphere_sprite.texture = LoadTexture("assets/Plasma_Sphere_Sheet.png");
        gs->cluster_sprite.texture       = LoadTexture("assets/Cluster_Sheet.png");
        gs->debrie_sprite.texture        = LoadTexture("assets/Debris.png");
        gs->ship.sprite.texture          = LoadTexture("assets/Retronian_Ship_Sheet.png");
        gs->ship.field_sprite.texture    = LoadTexture("assets/CounterField_Sheet.png");
        gs->laser_sprite.texture         = LoadTexture("assets/Laser_Sheet.png");
        gs->ship.flare_sprite.texture    = LoadTexture("assets/Flare_Sheet.png");
        gs->bfl_sprite.texture           = LoadTexture("assets/B.F.L_Sheet.png");
        gs->plasma_laser_sprite.texture  = LoadTexture("assets/Plasma_Laser_Sheet.png");
        gs->laser_bit_sprite.texture     = LoadTexture("assets/LaserBit_Sheet.png");
        gs->boomerang_sprite.texture     = LoadTexture("assets/Plasma_Seeker_Sheet.png");
        gs->great_plasma_laser_sprite.texture = LoadTexture("assets/Great_Plasma_Laser_Sheet.png");
        gs->terminals_sprite.texture     = LoadTexture("assets/Terminal_Pair_Sheet.png");
        gs->upper_cannon_sprite.texture  = LoadTexture("assets/Auric_Upper_Cannon_Sheet.png");
        gs->center_cannon_sprite.texture = LoadTexture("assets/Auric_Cannon_Center_Sheet.png");
        gs->lower_cannon_sprite.texture  = LoadTexture("assets/Auric_Lower_Cannon_Sheet.png");
        gs->mini_boss.sprite.texture     = LoadTexture("assets/GreatSeeker_Sheet.png");
        gs->boss_core_sprite.texture     = LoadTexture("assets/Auric_Heart_Sheet.png");

        gs->hud_font_size = 32;
        gs->hud_detail_font_size = 16;
        gs->hud_font = LoadFontEx("assets/MorePerfectDOSVGA.ttf", gs->hud_font_size, NULL, 0);
        gs->hud_detail_font = LoadFontEx("assets/MorePerfectDOSVGA.ttf", gs->hud_detail_font_size, NULL, 0);

        gs->current_music = LoadMusicStream("assets/music/Map.wav");

        gs->punch_sound_0 = LoadSound("assets/sounds/punch_0.wav");
        gs->flare_sound   = LoadSound("assets/sounds/hit_flare.wav");
        gs->hurt_sound    = LoadSound("assets/sounds/hurt.wav");
        gs->laser_sound   = LoadSound("assets/sounds/laser.wav");
        gs->bfl_sound     = LoadSound("assets/sounds/bfl.wav");

        gs->cluster_explosion_sound = LoadSound("assets/sounds/cluster_explosion.wav");

    } else {
        gs->current_music = LoadMusicStream("assets/music/Mercury.wav");
    }

    // Init entity tables
    gs->enemies_qnt     = 1;
    gs->enemies_last_id = 1;
    gs->projectiles_qnt     = 1;
    gs->projectiles_last_id = 1;

    for (int i = 0; i < MAX_ENEMIES; i++) {
        gs->enemies[i] = {};
    }

    for (int i = 0; i < MAX_PROJECTILES; i++) {
        gs->projectiles[i] = {};
    }

    gs->test_sprite.column_count = 2;
    gs->test_sprite.animation_count = 1;
    gs->test_sprite.frame_time = 1.0 / 10.0;
    gs->test_sprite.frames_count[0] = 2;

    gs->seeker_sprite.column_count = 6;
    gs->seeker_sprite.animation_count = 3;
    gs->seeker_sprite.frame_time = 1.0 / 10.0;
    gs->seeker_sprite.frames_count[0] = 6;
    gs->seeker_sprite.frames_count[1] = 4;
    gs->seeker_sprite.frames_count[2] = 6;

    gs->auric_ship_sprite.column_count = 6;
    gs->auric_ship_sprite.animation_count = 2;
    gs->auric_ship_sprite.frame_time = 1.0 / 10.0;
    gs->auric_ship_sprite.frames_count[0] = 6;

    gs->plasma_sphere_sprite.column_count = 6;
    gs->plasma_sphere_sprite.animation_count = 1;
    gs->plasma_sphere_sprite.frame_time = 1.0 / 10.0;
    gs->plasma_sphere_sprite.frames_count[0] = 6;

    gs->cluster_sprite.column_count = 50;
    gs->cluster_sprite.animation_count = 1;
    gs->cluster_sprite.frame_time = 1.0 / 10.0;
    gs->cluster_sprite.frames_count[0] = 50;

    gs->debrie_sprite.column_count    = 1;
    gs->debrie_sprite.animation_count = 1;
    gs->debrie_sprite.frame_time      = 1.0 / 10.0;
    gs->debrie_sprite.frames_count[0] = 1;

    gs->ship.sprite.column_count = 8;
    gs->ship.sprite.animation_count = 7;
    gs->ship.sprite.frame_time = 1.0 / 10.0;
    gs->ship.sprite.frames_count[0] = 8;
    gs->ship.sprite.frames_count[1] = 6;
    gs->ship.sprite.frames_count[2] = 6;
    gs->ship.sprite.frames_count[3] = 8;
    play_animation(&gs->ship.sprite, 0);

    gs->ship.field_sprite.column_count = 6;
    gs->ship.field_sprite.animation_count = 4;
    gs->ship.field_sprite.frame_time = 1.0 / 10.0;
    gs->ship.field_sprite.frames_count[0] = 6;
    gs->ship.field_sprite.frames_count[1] = 3;
    gs->ship.field_sprite.frames_count[2] = 6;
    gs->ship.field_sprite.frames_count[3] = 4;
    play_animation(&gs->ship.field_sprite, 0);

    gs->laser_sprite.column_count = 16;
    gs->laser_sprite.animation_count = 4;
    gs->laser_sprite.frame_time = 1.0 / 10.0;
    gs->laser_sprite.frames_count[0] = 16;
    gs->laser_sprite.frames_count[1] = 16;

    gs->ship.laser_pointer_sprite = gs->laser_sprite;
    gs->ship.laser_body_sprite    = gs->laser_sprite;

    gs->ship.flare_sprite.column_count = 8;
    gs->ship.flare_sprite.animation_count = 1;
    gs->ship.flare_sprite.frame_time = 1.0 / 10.0;
    gs->ship.flare_sprite.frames_count[0] = 8;

    gs->bfl_sprite.column_count = 8;
    gs->bfl_sprite.animation_count = 3;
    gs->bfl_sprite.frame_time = 1.0 / 10.0;
    gs->bfl_sprite.frames_count[0] = 8;
    gs->bfl_sprite.frames_count[1] = 8;
    gs->bfl_sprite.frames_count[2] = 8;

    gs->ship.bfl_pointer_sprite = gs->bfl_sprite;
    gs->ship.bfl_body_sprite    = gs->bfl_sprite;
    gs->ship.lives = 3;

    gs->plasma_laser_sprite.column_count = 8;
    gs->plasma_laser_sprite.animation_count = 4;
    gs->plasma_laser_sprite.frame_time = 1.0 / 10.0;
    gs->plasma_laser_sprite.frames_count[0] = 8;
    gs->plasma_laser_sprite.frames_count[1] = 8;
    gs->plasma_laser_sprite.frames_count[2] = 8;
    gs->plasma_laser_sprite.frames_count[3] = 8;

    gs->laser_bit_sprite.column_count = 6;
    gs->laser_bit_sprite.animation_count = 4;
    gs->laser_bit_sprite.frame_time = 1.0 / 10.0;
    gs->laser_bit_sprite.frames_count[0] = 3;
    gs->laser_bit_sprite.frames_count[1] = 6;
    gs->laser_bit_sprite.frames_count[2] = 3;
    gs->laser_bit_sprite.frames_count[3] = 6;

    gs->boomerang_sprite.column_count = 4;
    gs->boomerang_sprite.animation_count = 1;
    gs->boomerang_sprite.frame_time = 1.0 / 10.0;
    gs->boomerang_sprite.frames_count[0] = 4;

    gs->great_plasma_laser_sprite.column_count = 8;
    gs->great_plasma_laser_sprite.animation_count = 4;
    gs->great_plasma_laser_sprite.frame_time = 1.0 / 10.0;
    gs->great_plasma_laser_sprite.frames_count[0] = 6;
    gs->great_plasma_laser_sprite.frames_count[1] = 8;
    gs->great_plasma_laser_sprite.frames_count[2] = 6;
    gs->great_plasma_laser_sprite.frames_count[3] = 8;

    gs->terminals_sprite.column_count = 1;
    gs->terminals_sprite.animation_count = 2;
    gs->terminals_sprite.frame_time = 1.0 / 10.0;
    gs->terminals_sprite.frames_count[0] = 1;
    gs->terminals_sprite.frames_count[1] = 1;

    gs->upper_cannon_sprite.column_count = 16;
    gs->upper_cannon_sprite.animation_count = 1;
    gs->upper_cannon_sprite.frame_time = 1.0 / 10.0;
    gs->upper_cannon_sprite.frames_count[0] = 15;

    gs->center_cannon_sprite.column_count = 15;
    gs->center_cannon_sprite.animation_count = 1;
    gs->center_cannon_sprite.frame_time = 1.0 / 10.0;
    gs->center_cannon_sprite.frames_count[0] = 15;

    gs->lower_cannon_sprite.column_count = 16;
    gs->lower_cannon_sprite.animation_count = 1;
    gs->lower_cannon_sprite.frame_time = 1.0 / 10.0;
    gs->lower_cannon_sprite.frames_count[0] = 15;

    gs->boss_core_sprite.column_count = 25;
    gs->boss_core_sprite.animation_count = 4;
    gs->boss_core_sprite.frame_time = 1.0 / 10.0;
    gs->boss_core_sprite.frames_count[0] = 9;
    gs->boss_core_sprite.frames_count[1] = 6;
    gs->boss_core_sprite.frames_count[2] = 4;
    gs->boss_core_sprite.frames_count[3] = 25;

    // Set Initial values
    gs->ship.position = {50, 130};
    gs->ship.collider.type   = DShapeType_Rect;
    gs->ship.collider.r.size = vec2(18, 8);

    if (gs->dificulty == 0) {
        gs->ship.energy = 300;
    } else if (gs->dificulty == 1) {
        gs->ship.energy = 200;
    } else {
        gs->ship.energy = 100;
    }

    //gs->ship.energy = 10000;

    gs->ship.field_collider.type     = DShapeType_Circle;
    gs->ship.field_collider.c.radius = 27;
    gs->eflow.energy_in_last_cicle = gs->ship.energy;

    // Set Mini boss initial values
    gs->mini_boss.position       = vec2(225, 150);
    gs->mini_boss.vel_multiplier = 1.25;
    gs->mini_boss.sprite.animation_count = 3;
    gs->mini_boss.sprite.column_count = 50;
    gs->mini_boss.sprite.frame_time = 1.0 / 10.0;
    gs->mini_boss.sprite.frames_count[0] = 10;
    gs->mini_boss.sprite.frames_count[1] = 50;
    gs->mini_boss.sprite.frames_count[2] = 2;
    gs->mini_boss.collider.type = DShapeType_Rect;
    gs->mini_boss.collider.r.size = {50, 50};
    gs->mini_boss.hp = 80;
    gs->mini_boss.bomb_count = 1;
    gs->mini_boss.disabled = true;
    gs->mini_boss.state = MiniBossState_Initial;

    gs->hud_font_size = 32;
    gs->hud_detail_font_size = 16;

    gs->mugshot = 0;
    gs->debris_timer = 0;
    gs->explosion_x = 0;
    gs->explosion_beam_c = 0;

    for (int i = 0; i < 32; i++) {
        gs->explosion_beam_s[i] = gs->great_plasma_laser_sprite;
    }

    gs->upper_cannon_destroyed = false;
    gs->center_cannon_destroyed = false;
    gs->lower_cannon_destroyed = false;

    // Load sounds
    PlayMusicStream(gs->current_music);

    gs->level = 0;
}

internal void start_boss(bool enable);

internal void
reset_level() {
    if (!gs->reset) return;

    if (gs->ship.lives <= 0) {
        game_start(false);
    }

    start_boss(false);

    PlayMusicStream(gs->current_music);

    gs->reset       = false;
    gs->reset_timer = 0;

    // Resset wave
    gs->wave = 0;
    gs->wave_timer = 0;

    // Reset Ship
    gs->ship.position = {50, 130};
    
    if (gs->dificulty == 0) {
        gs->ship.energy = 300;
    } else if (gs->dificulty == 1) {
        gs->ship.energy = 200;
    } else {
        gs->ship.energy = 100;
    }

    gs->eflow.energy_in_last_cicle = gs->ship.energy;
    gs->eflow.cicle_cooldown = 0.5;

    // Init entity tables
    gs->enemies_qnt     = 1;
    gs->enemies_last_id = 1;
    for (int i = 0; i < MAX_ENEMIES; i += 1) gs->enemies[i].id = 0;

    gs->projectiles_qnt     = 1;
    gs->projectiles_last_id = 1;
    for (int i = 0; i < MAX_PROJECTILES; i += 1) gs->projectiles[i].id = 0;
    
    // Reset Mini boss    
    gs->mini_boss.position       = vec2(225, 150);
    gs->mini_boss.vel_multiplier = 1.25;
    gs->mini_boss.hp = 80;
    gs->mini_boss.bomb_count = 1;
    gs->mini_boss.disabled = true;
    gs->mini_boss.state = MiniBossState_Initial;

    gs->explosion_x = 0;
    gs->explosion_beam_c = 0;

    // Reset Cannon things
    gs->upper_cannon_destroyed  = false;
    gs->center_cannon_destroyed = false;
    gs->lower_cannon_destroyed  = false;

    gs->mugshot = 0;
    gs->mugshot = 0;
    gs->debris_timer = 0;
    gs->explosion_x = 0;
    gs->explosion_beam_c = 0;
}

internal u16
add_enemy(Enemy e) {
    if (gs->enemies_qnt >= MAX_ENEMIES) return 0;

    if (gs->enemies_last_id == gs->enemies_qnt) {
        e.id = gs->enemies_qnt;
        gs->enemies[gs->enemies_qnt] = e;
        gs->enemies_qnt += 1;
        gs->enemies_last_id += 1;

        return e.id;
    }

    for (int i = 1; i <= gs->enemies_last_id; i += 1) {
        if (gs->enemies[i].id == 0) {
            e.id = i;
            gs->enemies[i] = e;
            gs->enemies_qnt += 1;
            if (i == gs->enemies_last_id) gs->enemies_last_id += 1;
            break;
        }
    }

    return e.id;
}

internal void
add_projectile(Projectile p) {
    if (gs->projectiles_qnt >= MAX_PROJECTILES) return;

    if (gs->projectiles_last_id == gs->projectiles_qnt) {
        p.id = gs->projectiles_qnt;
        gs->projectiles[gs->projectiles_qnt] = p;
        gs->projectiles_qnt += 1;
        gs->projectiles_last_id += 1;

        return;
    }

    for (int i = 1; i <= gs->projectiles_last_id; i += 1) {
        if (gs->projectiles[i].id == 0) {
            p.id = i;
            gs->projectiles[i]  =  p;
            gs->projectiles_qnt += 1;
            if (i == gs->projectiles_last_id) gs->projectiles_last_id += 1;
            break;
        }
    }
}

internal bool
damage_ship(float damage) {
    if (gs->dificulty == 0) damage *= 0.5;
    if (gs->dificulty == 1) damage *= 0.75;

    if (gs->ship.damage_cooldown <= 0) {
        gs->ship.energy -= damage;
        gs->ship.damage_cooldown = 0.25;
        play_animation(&gs->ship.field_sprite, 2, false);

        PlaySound(gs->hurt_sound);
        
        if (gs->ship.energy <= 0) {
            gs->reset_timer = 5;
            gs->reset       = true;
            StopMusicStream(gs->current_music);

            gs->ship.energy = 0;

            gs->mugshot = 4; 

            create_random_particles(100, gs->ship.position, vec2(5, 5), vec2(50, 50), GREEN, GRAY);
            PlaySound(gs->cluster_explosion_sound);

            gs->ship.lives--;
        }

        return true;
    }

    return false;
}

internal bool
damage_enemy(Enemy *e, float hit_points) {
    if (e->hit_cooldown <= 0 && e->state != EnemyState_Protected) {
        e->hp -= hit_points;
        if(gs->ship.powerup_active_cooldown <= 0){
            gs->ship.energy +=2;
        }
        e->hit_cooldown = 0.15;
        if (e->hp <= 0) {
            create_random_particles(10, e->position, vec2(5,5), vec2(30,30), WHITE, LIGHTGRAY,
                true, vec2(5,5));
            gs->enemies_delete_queue.push(e->id);
            switch(e->type){
                case EnemyType_Upper_Cannon:  gs->upper_cannon_destroyed  = true; break;
                case EnemyType_Center_Cannon: gs->center_cannon_destroyed = true; break;
                case EnemyType_Lower_Cannon:  gs->lower_cannon_destroyed  = true; break;
                case EnemyType_Boss_Core:     gs->boss.defeated           = true; break;
            }
        }
        return true;
    }
    return false;
}

internal bool
damage_mini_boss(float damage) {
    if (gs->mini_boss.disabled) return false;

    if (gs->mini_boss.damage_cooldown < 0.15) {
        return false;
    }

    gs->mini_boss.damage_cooldown = 0;

    gs->mini_boss.hp -= damage;
    gs->ship.energy += 2;
    if (gs->mini_boss.hp <= 0.0) {
        create_random_particles(50, gs->mini_boss.position, vec2(25, 25), vec2(50, 50), ORANGE, PURPLE);
        gs->mini_boss.disabled = true;
        gs->level++;
        gs->wave = 0;
        gs->wave_timer = 0;
        gs->change_background = true;
        gs->bg3_scroll_x = -900;
        gs->bg4_scroll_x = -900;
    }

    return true;
}

#include "enemies.cpp"
#include "waves.cpp"
#include "boss.cpp"

internal void
update_enemies() {
    for (int i = 1; i < gs->enemies_last_id; i += 1) {
        Enemy *it = &gs->enemies[i];
        if (it->id == 0) continue;

        if (it->position.x < -500 || it->position.x > (S_WIDTH  * 0.5 + 500) ||
            it->position.y < -500 || it->position.y > (S_HEIGHT * 0.5 + 500))
        {
            printf("Enemy deleted!\n");
            gs->enemies_delete_queue.push(it->id);
            continue;
        }

        switch (it->type) {
        case EnemyType_Seeker:    update_seeker(it);     break;
        case EnemyType_BasicShip: update_basic_ship(it); break;
        case EnemyType_Cluster:   update_cluster(it);    break;
        case EnemyType_LaserBit:  update_laser_bit(it);  break;
        case EnemyType_Boomerang: update_boomerang(it);  break;
        case EnemyType_Debris:    update_debries(it);    break;
        case EnemyType_Upper_Cannon:    
        case EnemyType_Center_Cannon:
        case EnemyType_Lower_Cannon:update_cannon(it);    break;
        case EnemyType_Boss_Core: update_boss_core(it); break;
        }

        it->hit_cooldown -= delta;
        it->damage_again_timer += delta;
        it->collider.r.position = it->position;
        if (collision(it->collider, gs->ship.collider) && it->damage_again_timer > 0.5) {
            if (damage_ship(it->damage)) {
                it->damage_again_timer = 0;
            }
        }
    }
}

internal void
update_projectiles() {
    for (int i = 1; i < gs->projectiles_last_id; i += 1) {
        Projectile *it = &gs->projectiles[i];
        if (it->id == 0) continue;

        it->timer += delta;

        if (it->timer > PROJECTILE_TTL_SECS) {
            gs->projectiles_delete_queue.push(it->id);
            continue;
        }

        it->position += it->direction * it->speed * delta;
        it->collider.r.position = it->position;

        // Damage Ship
        if (collision(it->collider, gs->ship.collider) && damage_ship(it->damage)) {
            it->position += it->direction * it->speed * delta;
            gs->projectiles_delete_queue.push(it->id);

            create_random_particles(5, it->position, vec2(10,10), vec2(5,5), DARKGRAY, DARKGREEN);
        }
    }
}

internal void 
ship_update() {
    Ship *s = &gs->ship;

    Vec2 throttle = {};

    throttle.x += IsKeyDown(KEY_RIGHT) - IsKeyDown(KEY_LEFT);
    throttle.y += IsKeyDown(KEY_UP)    - IsKeyDown(KEY_DOWN);

    throttle = normalize(throttle);

    Vec2 next_pos = s->position + throttle * delta * 120;
    if (next_pos.x > 20 && next_pos.x < 430) {
        s->position.x = next_pos.x;
    }
    if(next_pos.y > 5 && next_pos.y < 245) { 
        s->position.y = next_pos.y;
    }

    s->collider.r.position     = s->position + vec2(11, 0);
    s->field_collider.c.center = s->position;

    bool punching   = false;
    bool atacking   = false;
    bool on_powerup = s->powerup_active_cooldown > 0;


    if (on_powerup) {
        atacking = true;
        gs->mugshot = 1;

        switch (s->actual_powerup) {
        case ShipPowerup_Laser: {
            DShape pc;
            pc.type       = DShapeType_Rect;
            pc.r.size     = vec2(S_WIDTH - s->position.x, 4);
            pc.r.position = s->position + vec2(pc.r.size.x/2, -2);
            
            if (!IsSoundPlaying(gs->laser_sound)) {
                PlaySound(gs->laser_sound);
            }

            for (int i = 1; i < gs->enemies_last_id; i += 1) {
                Enemy *it = &gs->enemies[i];
                if (it->id == 0) continue;

                if (collision(it->collider, pc)) damage_enemy(it, 0.5);
            }

            if (gs->mini_boss.disabled == false && collision(gs->mini_boss.collider, pc)) {
                damage_mini_boss(0.5);
            }

            check_and_damage_terminals(pc, 0.5);

            play_animation(&s->field_sprite, 2, false);
        } break;

        case ShipPowerup_Flare: {
            punching = true;

            play_animation(&s->sprite, 3);

            if (s->sprite.frame % 2 == 0) {
                DShape pc;
                pc.type       = DShapeType_Rect;
                pc.r.position = s->position + vec2(35, 0);
                pc.r.size     = vec2(20, 40);

                draw_rect(pc.r.position, pc.r.size, RED);

                for (int i = 1; i < gs->enemies_last_id; i += 1) {
                    Enemy *it = &gs->enemies[i];
                    if (it->id == 0) continue;

                    if (collision(it->collider, pc)) {
                        damage_enemy(it, 2);
                        PlaySound(gs->flare_sound);
                    }
                }

                if (gs->mini_boss.disabled == false && collision(gs->mini_boss.collider, pc)) {
                    damage_mini_boss(2);
                    PlaySound(gs->flare_sound);
                }

                if (check_and_damage_terminals(pc, 2)) {
                    PlaySound(gs->flare_sound);
                }
            }

            play_animation(&s->field_sprite, 2, false);
        } break;
        case ShipPowerup_BFL: {
            gs->mugshot = 2;

            if (s->bfl_pointer_sprite.animation == 0) {
                if (s->bfl_pointer_sprite.has_finished) {
                    play_animation(&s->bfl_pointer_sprite, 1, true);
                    play_animation(&s->bfl_body_sprite   , 2, true);
                }
            } else {
                if (!IsSoundPlaying(gs->bfl_sound)) {
                    PlaySound(gs->bfl_sound);
                }

                DShape pc;
                pc.type       = DShapeType_Rect;
                pc.r.size     = vec2(S_WIDTH - s->position.x, 16);
                pc.r.position = s->position + vec2(pc.r.size.x * 0.5f + 17.0f + 16.0f, 0.0f);
                

                draw_rect(pc.r.position, pc.r.size, RED);

                for (int i = 1; i < gs->enemies_last_id; i += 1) {
                    Enemy *it = &gs->enemies[i];
                    if (it->id == 0) continue;

                    if (collision(it->collider, pc)) damage_enemy(it, 2.5);
                }

                if (gs->mini_boss.disabled == false && collision(gs->mini_boss.collider, pc)) {
                    damage_mini_boss(2.5);
                }

                check_and_damage_terminals(pc, 2.5);
            }
        } break;
        }
    } else {
        StopSound(gs->laser_sound);
        StopSound(gs->bfl_sound);
    }

    if (!on_powerup && IsKeyDown(KEY_A)) {
        atacking = true;
        punching = true;

        play_animation(&s->sprite, 3);

        if (s->sprite.frame % 2 == 0) {
            DShape pc;
            pc.type       = DShapeType_Rect;
            pc.r.position = s->position + vec2(35, 0);
            pc.r.size     = vec2(12, 20);

            draw_rect(pc.r.position, pc.r.size, RED);

            bool has_punched_something = false;
            for (int i = 1; i < gs->enemies_last_id; i += 1) {
                Enemy *it = &gs->enemies[i];
                if (it->id == 0) continue;

                if (collision(it->collider, pc)) {
                    damage_enemy(it, 1);
                    PlaySound(gs->punch_sound_0);
                    has_punched_something = true;
                }
            }

            if (gs->mini_boss.disabled == false && collision(gs->mini_boss.collider, pc)) {
                damage_mini_boss(1);
                has_punched_something = true;
                PlaySound(gs->punch_sound_0);
            }

            if (check_and_damage_terminals(pc, 2.5)) {
                has_punched_something = true;
                PlaySound(gs->punch_sound_0);
            }

            if (has_punched_something) {
                //s->energy += 2;
            }
        }

        play_animation(&s->field_sprite, 2, false);
    }

    if (!atacking) {
        if (s->field_sprite.animation == 2 && s->field_sprite.has_finished) {
            play_animation(&s->field_sprite, 3, false);
        }

        if (s->field_sprite.animation == 3 &&
            s->field_sprite.has_finished)
        {
            play_animation(&s->field_sprite, 0);
        }

        if (s->damage_cooldown > 0) {
            gs->mugshot = 3;
        } else {
            gs->mugshot = 0;
        }
    } else {

        if (s->actual_powerup == ShipPowerup_BFL && on_powerup) {
            gs->mugshot = 2;
        } else {
            gs->mugshot = 1;
        }
    }

    if (!punching) {
        if (IsKeyDown(KEY_UP)) {
            play_animation(&s->sprite, 1);
        } else if (IsKeyDown(KEY_DOWN)) {
            play_animation(&s->sprite, 2);
        } else {
            play_animation(&s->sprite, 0);
        }
    }

    if (!on_powerup) {
        if (IsKeyPressed(KEY_S) && s->energy > LASER_COST) {
            s->energy -= LASER_COST;
            s->powerup_active_cooldown = 3.0;
            s->actual_powerup = ShipPowerup_Laser;
            
            play_animation(&s->laser_pointer_sprite, 0, true);
            play_animation(&s->laser_body_sprite   , 1, true);
        } else if (IsKeyPressed(KEY_D) && s->energy > FLARE_COST) {
            s->energy -= FLARE_COST;
            s->powerup_active_cooldown = 3.0;
            s->actual_powerup = ShipPowerup_Flare;
            
            play_animation(&s->flare_sprite, 0, true);
            play_animation(&s->sprite, 3);
        } else if (IsKeyPressed(KEY_F) && s->energy > BFL_COST) {
            s->energy -= BFL_COST;
            s->powerup_active_cooldown = 3.0;
            s->actual_powerup = ShipPowerup_BFL;
            
            play_animation(&s->bfl_pointer_sprite, 0, false);
        }
    }

    s->powerup_active_cooldown -= delta;

    if (s->field_sprite.has_finished &&
        s->field_sprite.animation == 1)
    {
        play_animation(&s->field_sprite, 0);
    }
 
    if (s->field_sprite.animation == 0) {
        s->field_collider.c.radius = 27;

        float energy_gained = 0.0;
        for (int i = 1; i < gs->enemies_last_id; i += 1) {
            Enemy *it = &gs->enemies[i];
            if (it->id == 0) continue;

            if (collision(s->field_collider, it->collider)) {
                energy_gained += 10;

                for (int i = 0; i < 5; i += 1) {
                    Vec2 pos = it->position;
                    pos.x += (float)GetRandomValue(-15, 15);
                    pos.y += (float)GetRandomValue(-15, 15);
                    create_particle(pos, vec2(0,4), vec2(3,3), (Color){154, 250, 158, 255}, 1,
                        true, &s->position);
                }
            }
        }

        if (gs->mini_boss.disabled == false && collision(gs->mini_boss.collider, s->field_collider)) {
            energy_gained += 10;

            for (int i = 0; i < 5; i += 1) {
                Vec2 pos = gs->mini_boss.position;
                pos.x += (float)GetRandomValue(-15, 15);
                pos.y += (float)GetRandomValue(-15, 15);
                create_particle(pos, vec2(0,4), vec2(3,3), (Color){154, 250, 158, 255}, 1,
                    true, &s->position);
            }
        }

        for (int i = 0; i < gs->projectiles_last_id; i += 1) {
            Projectile *it = &gs->projectiles[i];
            if (it->id == 0) continue;

            if (collision(s->field_collider, it->collider)) {
                energy_gained += 5;

                for (int i = 0; i < 5; i += 1) {
                    Vec2 pos = it->position;
                    pos.x += (float)GetRandomValue(-15, 15);
                    pos.y += (float)GetRandomValue(-15, 15);
                    create_particle(pos, vec2(0,4), vec2(3,3), (Color){154, 250, 158, 255}, 1,
                        true, &s->position);
                }
            }
        }

        if (energy_gained > 0) {
            play_animation(&s->field_sprite, 1, false);
        }

        s->energy += energy_gained;
        gs->eflow.energy_earned_in_this_cicle += energy_gained;
    } else {
        if (s->field_sprite.animation != 1) {
            s->field_collider.c.radius = 0;
        }
    }
}

internal void
game_update() {

    if (IsKeyPressed(KEY_P)) {
        gs->paused = !gs->paused;
    }
    if (gs->paused) {
        if (IsKeyPressed(KEY_ENTER)) gs->paused = false;
        return;
    }

#if 0
    if (IsKeyPressed(KEY_W)) gs->debug_mode = !gs->debug_mode;

    if (IsKeyPressed(KEY_SPACE)) {
        spawn_static_seeker(vec2(500, (float)GetRandomValue(20, 230)), 1.0);
    }

    if (IsKeyPressed(KEY_LEFT_SHIFT)) {
        spawn_basic_ship(vec2(500, (float)GetRandomValue(20, 230)), 1.0);
    }

    if (IsKeyPressed(KEY_LEFT_CONTROL)) {
        spawn_cluster(vec2(500, (float)GetRandomValue(20, 230)), 1.0);
    }

    if (IsKeyPressed(KEY_Q)) {
        spawn_laser_bit(vec2(500, (float)GetRandomValue(20, 230)), 1.0);
    }

    if (IsKeyPressed(KEY_E)) {
        spawn_boomerang(vec2(500, (float)GetRandomValue(20, 230)), 1.0);
    }

    if (IsKeyPressed(KEY_B)) {
        start_boss(true);
    }
    if (IsKeyPressed(KEY_C)) {
        spawn_upper_cannon(vec2(450-90 ,125+50), 1);
        spawn_center_cannon(vec2(450-113 ,125), 1);
        spawn_lower_cannon(vec2(450-90 ,125-50), 1);
    }

    if (IsKeyPressed(KEY_J)) {
        spawn_debrie(vec2(460, 125));
    }
#endif

    if (gs->reset_timer <= 0 && gs->level < 4) {
        ship_update();
    }

    if (gs->level == 4) {
        gs->ship.position.x += delta * 100;

        if (gs->ship.position.x > 460) {
            menu_state = MenuState_Credits;
        }
    }

    if (gs->eflow.cicle_cooldown > 0.5) {
        float actual_energy = gs->ship.energy;
        float delta_energy  = actual_energy - gs->eflow.energy_in_last_cicle;
        float energy_lost   = gs->eflow.energy_earned_in_this_cicle - delta_energy;

        if (energy_lost != 0 || gs->eflow.energy_earned_in_this_cicle != 0) {
            for (int i = 0; i < (EFLOW_ENTRIES - 1); i += 1) {
                gs->eflow.positive_values[i] = gs->eflow.positive_values[i + 1];
                gs->eflow.negative_values[i] = gs->eflow.negative_values[i + 1];
            }

            gs->eflow.positive_values[EFLOW_ENTRIES - 1] = gs->eflow.energy_earned_in_this_cicle;
            gs->eflow.negative_values[EFLOW_ENTRIES - 1] = energy_lost;

            gs->eflow.energy_in_last_cicle = actual_energy;
            gs->eflow.energy_earned_in_this_cicle = 0;
            gs->eflow.cicle_cooldown = 0;
        }
    }
    gs->eflow.cicle_cooldown += delta;

    update_mini_boss(&gs->mini_boss);
    update_boss();
    update_enemies();
    update_projectiles();

    // Update Explosion
    if (gs->level == 3) {
        gs->explosion_x += delta * 10;

        if (gs->explosion_x <= 350) {
            DShape ec   = {};
            ec.type     = DShapeType_Rect;
            ec.r.position.y = 125;
            ec.r.size   = {gs->explosion_x * 2, 250};

            if (collision(ec, gs->ship.field_collider)) {
                gs->ship.energy += delta * 5;
                gs->eflow.energy_earned_in_this_cicle += delta * 10;
            }

            if (gs->ship.position.x < gs->explosion_x) {
                damage_ship(999.0f);
                //gs->explosion_x = 0;
            }

            for (int i = 0; i < gs->explosion_beam_c; i++) {
                gs->explosion_beam_t[i] -= delta;

                if (gs->explosion_beam_t[i] <= 5) {
                    DShape ec = {};
                    ec.type = DShapeType_Rect;
                    ec.r.position = vec2(0, gs->explosion_beam_y[i]);
                    ec.r.size     = vec2(900, 26);

                    if (collision(ec, gs->ship.collider)) {
                        damage_ship(999999);
                    }

                    if (collision(ec, gs->ship.field_collider)) {
                        gs->ship.energy += delta * 5;
                        gs->eflow.energy_earned_in_this_cicle += delta * 10;
                    }
                }
                
            }
        }

        // Destroy explosion beam
        for (int i = 0; i < gs->explosion_beam_c; i++) {
            if (gs->explosion_beam_t[i] <= 0) {
                gs->explosion_beam_c--;
                gs->explosion_beam_y[i] = gs->explosion_beam_y[gs->explosion_beam_c];
                gs->explosion_beam_t[i] = gs->explosion_beam_t[gs->explosion_beam_c];
            }
        }
    }

    while (gs->enemies_delete_queue.count > 0) {
        int id = gs->enemies_delete_queue.pop();
        gs->enemies[id] = {};
        gs->enemies_qnt -= 1;
        if ((gs->enemies_last_id - 1) == id) gs->enemies_last_id -= 1;
    }

    while (gs->projectiles_delete_queue.count > 0) {
        int id = gs->projectiles_delete_queue.pop();
        gs->projectiles[id] = {};
        gs->projectiles_qnt -= 1;
        if ((gs->projectiles_last_id - 1) == id) gs->projectiles_last_id -= 1;
    }

    if (gs->level == 0) {
        wave_update();
    } else if (gs->level == 1) {
        wave2_update();
    } else if (gs->level == 3) {
        if (gs->debris_timer <= 0) {
            spawn_debrie(vec2(480, (float)GetRandomValue(20, 230)));
            gs->debris_timer = GetRandomValue(1, 3);
        }
        gs->debris_timer -= delta;
    }

    if (gs->reset_timer <= 0) { 
        reset_level();
        //start_boss(false);
    }
    gs->reset_timer -= delta;
}

internal void 
render_enemies() {
    for (int i = 1; i < gs->enemies_last_id; i += 1) {
        Enemy *it = &gs->enemies[i];
        if (it->id == 0) continue;

        //draw_rect(it->collider.r.position, it->collider.r.size, BLUE);
        if (it->hit_cooldown <= 0 || (total_frame_count % 6 == 0)) {
            if (it->type == EnemyType_LaserBit ) {
                if (it->sprite.animation == 0) {
                    int i = 0;
                    Vec2 it_pos = {};
                    it_pos.x = it->position.x;
                    while (true) {
                        draw_sprite(&it->laser_sprite, it_pos, !i);
                        i += 1;
                        it_pos.y += 8;
                        if (it_pos.y > S_HEIGHT) break;
                    }

                } else if (it->sprite.animation == 2) {
                    int i = 0;
                    Vec2 it_pos = {};
                    it_pos.y = it->position.y;
                    while (true) {
                        draw_sprite(&it->laser_sprite, it_pos, !i);
                        i += 1;
                        it_pos.x += 8;
                        if (it_pos.x > S_WIDTH) break;
                    }
                }
            }else if(it->type == EnemyType_Upper_Cannon  ||
                     it->type == EnemyType_Center_Cannon ||
                     it->type == EnemyType_Lower_Cannon    ){
                int i = 0;
                Vec2 it_pos = {};
                it_pos = it->position;
                switch(it->state){
                    case EnemyState_Aiming: 
                    case EnemyState_Shooting:{
                        while (true) {
                            draw_sprite(&it->laser_sprite, it_pos + vec2(-12,0), !i);
                            i += 1;
                            it_pos.x -= 32;
                            if (it_pos.x < -32) break;
                        }
                    } break;
                }
            }

            if (gs->debug_mode) {
                draw_rect(it->collider.r.position, it->collider.r.size, BLUE);
            }

            draw_sprite(&it->sprite, it->position);
        }
    }
}

internal void
render_projectiles() {
    for (int i = 1; i < gs->projectiles_last_id; i += 1) {
        Projectile *it = &gs->projectiles[i];
        if (it->id == 0) continue;

        if (gs->debug_mode) {
            draw_rect(it->collider.r.position, it->collider.r.size, GREEN);
        }

        draw_sprite(&it->sprite, it->position);
    }
}

internal void
game_render() {
    UpdateMusicStream(gs->current_music);
    // Draw Background
    {
        // Far
        DrawTextureEx(gs->bg2_texture, {0, 100}, 0, 2, WHITE);

        if (gs->level == 0 || gs->change_background) {
            DrawTextureEx(gs->bg0_texture, {-(float)gs->bg0_scroll_x, 100}, 0, 2, WHITE);
            DrawTextureEx(gs->bg0_texture, {-(float)gs->bg0_scroll_x + 900, 100}, 0, 2, WHITE);
        } 

        if (gs->level == 1) {
            DrawTextureEx(gs->bg4_texture, {-(float)gs->bg4_scroll_x, 100}, 0, 2, WHITE);
            DrawTextureEx(gs->bg4_texture, {-(float)gs->bg4_scroll_x + 900 * 3, 100}, 0, 2, WHITE);

            DrawTextureEx(gs->bg3_texture, {-(float)gs->bg3_scroll_x, 100}, 0, 2, WHITE);
            DrawTextureEx(gs->bg3_texture, {-(float)gs->bg3_scroll_x + 900, 100}, 0, 2, WHITE);

            if (!gs->paused) {
                gs->bg3_scroll_x += delta * 400;
                gs->bg4_scroll_x += delta * 300;
                if (gs->bg3_scroll_x >= 900) gs->bg3_scroll_x -= 900;
                if (gs->bg4_scroll_x >= 900 * 3) gs->bg4_scroll_x -= 900 * 3;
            }
        }

        if (!gs->paused) {
            gs->bg0_scroll_x += delta * 400;
            if (gs->change_background) {
                if (gs->bg0_scroll_x > 1800) {
                    gs->change_background = false;
                    gs->current_music = LoadMusicStream("assets/music/Venus.wav");
                    PlayMusicStream(gs->current_music);
                }
            } else {
                if (gs->bg0_scroll_x >= 900) gs->bg0_scroll_x -= 900;
            }
        }

        if (gs->level == 2) {
            DrawTextureEx(gs->bg4_texture, {0, 100}, 0, 2, WHITE);
            DrawTextureEx(gs->bg3_texture, {0, 100}, 0, 2, WHITE);
        }

        if (gs->level == 3) {
            DrawTextureEx(gs->bg4_texture, {-(float)gs->bg4_scroll_x, 100}, 0, 2, WHITE);
            DrawTextureEx(gs->bg4_texture, {-(float)gs->bg4_scroll_x + 900 * 3, 100}, 0, 2, WHITE);

            DrawTextureEx(gs->bg3_texture, {-(float)gs->bg3_scroll_x, 100}, 0, 2, WHITE);
            DrawTextureEx(gs->bg3_texture, {-(float)gs->bg3_scroll_x + 900, 100}, 0, 2, WHITE);

            gs->bg3_scroll_x += delta * 400;
            gs->bg4_scroll_x += delta * 300;
            if (gs->bg3_scroll_x >= 900) gs->bg3_scroll_x -= 900;
            if (gs->bg4_scroll_x >= 900 * 3) gs->bg4_scroll_x -= 900 * 3;
        }

    }

    if (gs->paused) {
        DrawText("Press [Enter] to continue", 240, 260, 32, WHITE);
        return;
    }

    draw_particles();

	render_boss();

    // Draw Ship
    if (gs->reset_timer <= 0) {
        if (gs->debug_mode) draw_rect(gs->ship.collider.r.position, gs->ship.collider.r.size, RED);
        if (gs->ship.damage_cooldown <= 0 || (total_frame_count % 6 == 0)) {
            draw_sprite(&gs->ship.sprite, gs->ship.position);
        }
        gs->ship.damage_cooldown -= delta;

        draw_sprite(&gs->ship.field_sprite, gs->ship.position);

        if (gs->ship.powerup_active_cooldown > 0) {
            switch (gs->ship.actual_powerup) {
            case ShipPowerup_Laser: {
                Vec2 it_pos = gs->ship.position;
                it_pos.x += 17;
                draw_sprite(&gs->ship.laser_pointer_sprite, it_pos);
                persistent int laser_particle_t = 0;
                int i = 0;
                while (true) {
                    it_pos.x += 8;
                    draw_sprite(&gs->ship.laser_body_sprite, it_pos, !i);

                    if (laser_particle_t % 33 == 0) {
                        create_random_particles(1, it_pos, vec2(10,4), vec2(8,8), GREEN, DARKGREEN, true);
                    }

                    laser_particle_t += 1;
                    i += 1;
                    
                    if (it_pos.x > S_WIDTH) break;
                }
            } break;
            case ShipPowerup_Flare: {
                draw_sprite(&gs->ship.flare_sprite, gs->ship.position + vec2(16, 0));
            } break;
            case ShipPowerup_BFL: {
                Vec2 it_pos = gs->ship.position;
                it_pos.x += 17;
                draw_sprite(&gs->ship.bfl_pointer_sprite, it_pos);

                if (gs->ship.bfl_pointer_sprite.animation != 0) {
                    persistent int bfl_particle_t = 0;
                    int i = 0;
                    while (true) {
                        it_pos.x += 8;
                        draw_sprite(&gs->ship.bfl_body_sprite, it_pos, !i);
                        
                        if (bfl_particle_t % 33 == 0) {
                            create_random_particles(1, it_pos, vec2(10, 16), vec2(30,30), GREEN, (Color){154, 250, 158, 255}, true);
                            bfl_particle_t += 1;
                        }

                        bfl_particle_t += 1;
                        i += 1;

                        if (it_pos.x > S_WIDTH) break;
                    }
                }
            }
            }
        }
    }

    draw_mini_boss(&gs->mini_boss);
    render_enemies();
    render_projectiles();

    if (gs->level == 3) {
        for (int i = 0; i < gs->explosion_beam_c; i++) {
            gs->explosion_beam_t[i] -= delta;

            if (gs->explosion_beam_t[i] > 5) {
                play_animation(&gs->explosion_beam_s[i], 0);
            } else {
                play_animation(&gs->explosion_beam_s[i], 1);
            }

            for (int j = 0; j < 15; j++) {
                draw_sprite(&gs->explosion_beam_s[i],
                    vec2((float)j * 32, gs->explosion_beam_y[i]), !j);
            }
        }

        draw_rect(vec2(0, 0), vec2(gs->explosion_x * 2, 500), WHITE);
        DrawTextureEx(gs->explosion_texture, {gs->explosion_x * 2 - 50, 100}, 0, 2, WHITE);

        if (gs->explosion_x > 350) {
            DrawRectangle(0, 0, 900, 600, WHITE);
        }

        if (gs->explosion_x > 360) {
            gs->level = 4;
            gs->ship.position = vec2(50, 125);
        }
    }

    if (gs->level == 1) {
        if (gs->wave == 16 && gs->wave_timer > 149) {
            DrawRectangle(0, 0, 900, 600, WHITE);
        }
    }

    if (gs->level == 2) {
        if (gs->boss.defeated && gs->boss.defeated_timer > 4) {
            DrawRectangle(0, 0, 900, 600, WHITE);
        }
    }


    { // HUD
        DrawRectangle(0, 0, 900, 100, BLACK);

        char buffer[80];
        int int_energy = (int)ceil(gs->ship.energy);
        snprintf(buffer, 80, "%d%%", int_energy);
        //DrawText(buffer, 425, 40, 25, GREEN);

        DrawTextEx(gs->hud_font, buffer, (Vector2){415, 35}, gs->hud_font_size, 1.0, GREEN);

        snprintf(buffer, 80, "lives: %d", gs->ship.lives);
        DrawTextEx(gs->hud_detail_font, buffer, (Vector2){ 411, 72}, gs->hud_detail_font_size, 1.0, GREEN);

        int x_offset = 6;

        for (int i = 1; i < EFLOW_ENTRIES; i += 1) {
            float p = gs->eflow.positive_values[i] / 2;
            DrawRectangle(105 + x_offset + i*10 - 10, 50 - p, 8, p, GREEN);

            float n = gs->eflow.negative_values[i] / 2;
            Color c;
            if (n > 45) {
                c = WHITE;
                n = 45;
            } else {
                c = RED;
            }
            DrawRectangle(105 + x_offset + i*10 - 10, 50, 8, n, c);
        }

        float p = 0;
        DrawTexturePro(gs->mugshot_texture,
            {(float)gs->mugshot * 100, 0, 100, 100},
            {0, 0, 100, 100}, {}, 0, WHITE);
        p += 100;

        //DrawRectangleLinesEx((Rectangle){ p + 5, 5, EFLOW_ENTRIES * 10, 90}, 2, DARKGREEN);
        p += EFLOW_ENTRIES * 10 + 5;

        {
            //DrawRectangleLinesEx((Rectangle){ p + 5, 5, 100, 90}, 2, DARKGREEN);

            int size = (125 - 20) / 3;

            /*DrawRectangle(, DARKGREEN);
            DrawRectangle(105 + x_offset + i*10 - 10, 50, 8, n, DARKGREEN);
            DrawRectangle(105 + x_offset + i*10 - 10, 50, 8, n, DARKGREEN);*/

            p += 100 + 5;
        }


        //DrawRectangleLinesEx((Rectangle){ p + 5, 5, 127, 90}, 2, DARKGREEN);

        Color c = gs->ship.energy <= 50 ? DARKGREEN : GREEN;

        DrawTextEx(gs->hud_font, "Laser", (Vector2){ p + 25, 7}, gs->hud_font_size, 1.0, c);
        DrawTextEx(gs->hud_font, "50%", (Vector2){ p + 42, 39}, gs->hud_font_size, 1.0, c);
        DrawTextEx(gs->hud_detail_font, "key: S", (Vector2){ p + 40, 72}, gs->hud_detail_font_size, 1.0, c);
        p += 127 + 5;

         c = gs->ship.energy <= 30 ? DARKGREEN : GREEN;


        //DrawRectangleLinesEx((Rectangle){ p + 5, 5, 127, 90}, 2, DARKGREEN);
        DrawTextEx(gs->hud_font, "Flare", (Vector2){ p + 25, 7}, gs->hud_font_size, 1.0, c);
        DrawTextEx(gs->hud_font, "30%", (Vector2){ p + 42, 39}, gs->hud_font_size, 1.0, c);
        DrawTextEx(gs->hud_detail_font, "key: D", (Vector2){ p + 40, 72}, gs->hud_detail_font_size, 1.0, c);
        p += 127 + 5;

        c = gs->ship.energy <= 100 ? DARKGREEN : GREEN;


        //DrawRectangleLinesEx((Rectangle){ p + 5, 5, 127, 90}, 2, DARKGREEN);
        DrawTextEx(gs->hud_font, "BFL", (Vector2){ p + 42, 7}, gs->hud_font_size, 1.0, c);
        DrawTextEx(gs->hud_font, "100%", (Vector2){ p + 35, 39}, gs->hud_font_size, 1.0, c);
        DrawTextEx(gs->hud_detail_font, "key: F", (Vector2){ p + 40, 72}, gs->hud_detail_font_size, 1.0, c);
        p += 127 + 5;

        DrawTextureEx(gs->hud, {0, 0}, 0, 2, WHITE);
    }

    //DrawFPS(15, 575);
}
