struct Sprite {
	Texture texture;

	int column_count;
	int animation_count;

	int frames_count[8];
	int frame_count;

	int frame;
	int animation;
	b32 loop;
	b32 has_finished;

	f32 timer;
	f32 frame_time;
};

internal void
play_animation(Sprite *sprite, int animation, b32 loop = true) {
	//assert(animation <= sprite->animation_count);

	if (animation >= sprite->animation_count) return;

	if (animation != sprite->animation) {
		sprite->frame = 0;
		sprite->animation = animation;
		sprite->has_finished = false;
	}

	sprite->loop = loop;

	if (sprite->loop) {
		sprite->has_finished = false;
	}

	sprite->frame_count = sprite->frames_count[animation];
}

internal void
draw_sprite(Sprite *sprite, Vec2 position, bool update = true, bool draw = true) {
	if (update) {
		sprite->timer += delta;
		if (sprite->timer >= sprite->frame_time) {
			sprite->frame++;

			if (sprite->frame >= sprite->frame_count) {
				if (sprite->loop) {
					sprite->frame = 0;
				} else {
					sprite->frame = sprite->frame_count - 1;
					sprite->has_finished = true;
				}
			}

			sprite->timer -= sprite->frame_time;
		}
	}

	if (draw) {
		assert(sprite->texture.width  % sprite->column_count    == 0);
		assert(sprite->texture.height % sprite->animation_count == 0);

		float frame_width  = sprite->texture.width  / sprite->column_count;
		float frame_height = sprite->texture.height / sprite->animation_count;

		Rectangle source = {frame_width * sprite->frame, frame_height * sprite->animation, frame_width, frame_height};
		Rectangle dest = {
			(position.x - frame_width * 0.5f) * 2.0f,
			S_HEIGHT - (position.y + frame_height * 0.5f) * 2.0f,
			frame_width  * 2,
			frame_height * 2
		};

		DrawTexturePro(sprite->texture, source, dest, {0, 0}, 0, WHITE);
	}
}