#include <raylib.h>
#include <stdio.h>
#include <stdint.h>
#include <assert.h>

typedef float  f32;
typedef double f64;

typedef uint8_t  u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

typedef int8_t  i8;
typedef int16_t i16;
typedef int32_t i32;
typedef int64_t i64;

typedef uint32_t b32;

#define internal   static
#define global     static
#define persistent static

#define S_WIDTH  900
#define S_HEIGHT 600

#define S_SCALE 2


enum MenuState {
    MenuState_MainMenu,
    MenuState_Game,
    MenuState_Credits,
};

// Globals
global f32 delta = 0.0;
global u64 total_frame_count = 0;
global MenuState menu_state;

// Unity build files
#include "dmath.h"
#include "util.cpp"
#include "animation.cpp"
#include "particles.cpp"

#include "game.cpp"

#if defined(PLATFORM_WEB)
    #include <emscripten/emscripten.h>
#endif

void update(void) {
    total_frame_count += 1;

    delta = GetFrameTime();
    // Draw
    BeginDrawing();
    ClearBackground(BLACK);

    switch (menu_state) {
    case MenuState_MainMenu: {

        DrawTextEx(gs->hud_font, "Star Brawler 2", (Vector2){ 64, 36}, gs->hud_font_size, 1.0, GREEN);

        // Draw Background
        DrawTextureEx(gs->bg2_texture, {0, 100}, 0, 2, WHITE);

        if (IsKeyPressed(KEY_UP)   && gs->dificulty > 0) gs->dificulty--;
        if (IsKeyPressed(KEY_DOWN) && gs->dificulty < 2) gs->dificulty++;
                
        DrawTextEx(gs->hud_font, "EASY", (Vector2){ 64, 300}, gs->hud_font_size, 1.0, 
            gs->dificulty == 0 ? YELLOW : GREEN);
        DrawTextEx(gs->hud_font, "MEDIUM", (Vector2){ 64, 330}, gs->hud_font_size, 1.0,
            gs->dificulty == 1 ? YELLOW : GREEN);
        DrawTextEx(gs->hud_font, "NORMAL", (Vector2){ 64, 360}, gs->hud_font_size, 1.0,
            gs->dificulty == 2 ? YELLOW : GREEN);

        DrawTextEx(gs->hud_font, "Press [Enter] to start", (Vector2){ 240, 470}, gs->hud_font_size, 1.0, GREEN);
        DrawTextEx(gs->hud_font, "Press [C] to go to credits", (Vector2){ 220, 470 + 80}, gs->hud_font_size, 1.0, GREEN);

        if (IsKeyPressed(KEY_ENTER)) {
            menu_state = MenuState_Game;
            game_start(false);
        }

        if (IsKeyPressed(KEY_C)) {
            menu_state = MenuState_Credits;
        }
        UpdateMusicStream(gs->current_music);
    } break;
    case MenuState_Game:
        game_update();
        game_render();
    break;
    case MenuState_Credits: {
        UpdateMusicStream(gs->current_music);

        DrawTextEx(gs->hud_font, "Credits:", (Vector2){ 64, 64}, gs->hud_font_size, 1.0, WHITE);

        DrawTextEx(gs->hud_font, "Samuel Deboni  - Programmer", (Vector2){ 128, 128}, gs->hud_font_size, 1.0, WHITE);
        DrawTextEx(gs->hud_font, "Igor Fagundes  - Programmer", (Vector2){ 128, 128 + 40}, gs->hud_font_size, 1.0, WHITE);
        DrawTextEx(gs->hud_font, "Gleiston Assis - Artist"    , (Vector2){ 128, 128 + 80}, gs->hud_font_size, 1.0, WHITE);
        DrawTextEx(gs->hud_font, "Music from: https://opengameart.org/content/nes-shooter-music-5-tracks-3-jingles"    , (Vector2){ 64, 128 + 140}, gs->hud_font_size * 0.5, 1.0, WHITE);
        DrawTextEx(gs->hud_font, "Made with Raylib", (Vector2){ 128, 128 + 180}, gs->hud_font_size, 1.0, WHITE);

        DrawTextEx(gs->hud_font, "Press [Enter] to return", (Vector2){ 128, 400 }, gs->hud_font_size, 1.0, WHITE);

        if (IsKeyPressed(KEY_ENTER)) {
            menu_state = MenuState_MainMenu;
        }
    } break; 
    }

    EndDrawing();
}


int main(void)
{
    InitWindow(S_WIDTH, S_HEIGHT, "Star Brawler 2");
    InitAudioDevice();

    while(!IsAudioDeviceReady()){}

    game_start();
    gs->dificulty = 2;
    PlayMusicStream(gs->current_music);
    SetTargetFPS(60);
#if defined(PLATFORM_WEB)
    emscripten_set_main_loop(update, 0, 1);
#else
    while (!WindowShouldClose()) {
        update(); 
    }
#endif
    CloseWindow();
    return 0;
}
