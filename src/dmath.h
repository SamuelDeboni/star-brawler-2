/* date = August 25th 2021 7:14 pm */
// NOTE(samuel): This is a c++ header

#ifndef DMATH_H
#define DMATH_H

#define DM_PI 3.141592653

#include <math.h>

#define KILOBYTE(x) ((x) * (u64)1024L)
#define MEGABYTE(x) (KILOBYTE(x) * (u64)1024L)
#define GIGABYTE(x) (MEGABYTE(x) * (u64)1024L)

//~ Scalar

inline f32 dm_sin(f32 f) {return sinf(f);}
inline f32 dm_cos(f32 f) {return cosf(f);}
inline f32 dm_tan(f32 f) {return tanf(f);}
inline f32 dm_sqrt(f32 f) {return sqrtf(f);}

//~
union Vec2i {
    struct { i32 x, y; };
    i32 arr[2];
};

union Vec3i {
    struct { i32 x, y, z; };
    struct { i32 r, g, b; };
    i32 arr[3];
};

union Vec2 {
    struct { f32 x, y; };
    f32 arr[2];
};

union Vec3 {
    struct { f32 x, y, z; };
    struct { f32 r, g, b; };
    struct { f32 _x; Vec2 yz; };
    Vec2 xy;
    f32 arr[3];
};

union Vec4 {
    struct { f32 x, y, z, w; };
    struct { f32 r, g, b, a; };
    struct { Vec2 xy, zw; };
    Vec3 xyz;
    f32 arr[4];
};

union Mat4 {
    struct { Vec4 x, y, z, w; };
    f32 m[4][4];
    f32 arr[16];
};

//~ NOTE(samuel): Constructors

//- Vec2i
inline Vec2i vec2i(i32 x, i32 y) {
    return Vec2i{x, y};
}

inline Vec2i vec2i(Vec2 vf) {
    return Vec2i{(i32)vf.x, (i32)vf.y};
}

//- Vec2
inline Vec2 vec2(f32 x, f32 y) {
    return Vec2{x, y};
}

inline Vec2 vec2(Vec2i vi) {
    return Vec2{(f32)vi.x, (f32)vi.y};
}

//- Vec3i
inline Vec3i vec3i(i32 x, i32 y, i32 z) {
    return Vec3i{x, y, z};
}

inline Vec3i vec3i(Vec3i vi) {
    return Vec3i{(i32)vi.x, (i32)vi.y, (i32)vi.z};
}

//- Vec3
inline Vec3 vec3(f32 x, f32 y, f32 z) {
    return Vec3{x, y, z};
}

inline Vec3 vec3(Vec3i vi) {
    return Vec3{(f32)vi.x, (f32)vi.y, (f32)vi.z};
}


//~ NOTE(samuel): Vec2 Vec2 operations
inline Vec2 operator+(Vec2 a, Vec2 b) { return Vec2{a.x + b.x, a.y + b.y}; }
inline Vec2 operator-(Vec2 a, Vec2 b) { return Vec2{a.x - b.x, a.y - b.y}; }
inline Vec2 operator*(Vec2 a, Vec2 b) { return Vec2{a.x * b.x, a.y * b.y}; }
inline Vec2 operator/(Vec2 a, Vec2 b) { return Vec2{a.x / b.x, a.y / b.y}; }

inline void operator+=(Vec2 &a, Vec2 b) { a = a + b; }
inline void operator-=(Vec2 &a, Vec2 b) { a = a - b; }
inline void operator*=(Vec2 &a, Vec2 b) { a = a * b; }
inline void operator/=(Vec2 &a, Vec2 b) { a = a / b; }

//~ NOTE(samuel): Vec2 f32 operations
inline Vec2 operator+(Vec2 a, f32 f) { return Vec2{a.x + f, a.y + f}; }
inline Vec2 operator-(Vec2 a, f32 f) { return Vec2{a.x - f, a.y - f}; }
inline Vec2 operator*(Vec2 a, f32 f) { return Vec2{a.x * f, a.y * f}; }
inline Vec2 operator/(Vec2 a, f32 f) { return Vec2{a.x / f, a.y / f}; }

//~ NOTE(samuel): f32 Vec2 operations
inline Vec2 operator+(f32 f, Vec2 a) { return Vec2{f + a.x, f + a.y}; }
inline Vec2 operator-(f32 f, Vec2 a) { return Vec2{f - a.x, f - a.y}; }
inline Vec2 operator*(f32 f, Vec2 a) { return Vec2{f * a.x, f * a.y}; }
inline Vec2 operator/(f32 f, Vec2 a) { return Vec2{f / a.x, f / a.y}; }

//~ NOTE(samuel): Vec2i Vec2i operations
inline Vec2i operator+(Vec2i a, Vec2i b) { return Vec2i{a.x + b.x, a.y + b.y}; }
inline Vec2i operator-(Vec2i a, Vec2i b) { return Vec2i{a.x - b.x, a.y - b.y}; }
inline Vec2i operator*(Vec2i a, Vec2i b) { return Vec2i{a.x * b.x, a.y * b.y}; }
inline Vec2i operator/(Vec2i a, Vec2i b) { return Vec2i{a.x / b.x, a.y / b.y}; }

inline void operator+=(Vec2i &a, Vec2i b) { a = a + b; }
inline void operator-=(Vec2i &a, Vec2i b) { a = a - b; }
inline void operator*=(Vec2i &a, Vec2i b) { a = a * b; }
inline void operator/=(Vec2i &a, Vec2i b) { a = a / b; }

//~ NOTE(samuel): Vec2i f32 operations
inline Vec2i operator+(Vec2i a, i32 f) { return Vec2i{a.x + f, a.y + f}; }
inline Vec2i operator-(Vec2i a, i32 f) { return Vec2i{a.x - f, a.y - f}; }
inline Vec2i operator*(Vec2i a, i32 f) { return Vec2i{a.x * f, a.y * f}; }
inline Vec2i operator/(Vec2i a, i32 f) { return Vec2i{a.x / f, a.y / f}; }

//~ NOTE(samuel): i32 Vec2i operations
inline Vec2i operator+(i32 f, Vec2i a) { return Vec2i{f + a.x, f + a.y}; }
inline Vec2i operator-(i32 f, Vec2i a) { return Vec2i{f - a.x, f - a.y}; }
inline Vec2i operator*(i32 f, Vec2i a) { return Vec2i{f * a.x, f * a.y}; }
inline Vec2i operator/(i32 f, Vec2i a) { return Vec2i{f / a.x, f / a.y}; }


//~ NOTE(samuel): Vec3 Vec3 operations
inline Vec3 operator+(Vec3 a, Vec3 b) { return Vec3{a.x + b.x, a.y + b.y, a.z + b.z}; }
inline Vec3 operator-(Vec3 a, Vec3 b) { return Vec3{a.x - b.x, a.y - b.y, a.z - b.z}; }
inline Vec3 operator*(Vec3 a, Vec3 b) { return Vec3{a.x * b.x, a.y * b.y, a.z * b.z}; }
inline Vec3 operator/(Vec3 a, Vec3 b) { return Vec3{a.x / b.x, a.y / b.y, a.z / b.z}; }

inline void operator+=(Vec3 &a, Vec3 b) { a = a + b; }
inline void operator-=(Vec3 &a, Vec3 b) { a = a - b; }
inline void operator*=(Vec3 &a, Vec3 b) { a = a * b; }
inline void operator/=(Vec3 &a, Vec3 b) { a = a / b; }

//~ NOTE(samuel): Vec3 f32 operations
inline Vec3 operator+(Vec3 a, f32 f) { return Vec3{a.x + f, a.y + f, a.z + f}; }
inline Vec3 operator-(Vec3 a, f32 f) { return Vec3{a.x - f, a.y - f, a.z - f}; }
inline Vec3 operator*(Vec3 a, f32 f) { return Vec3{a.x * f, a.y * f, a.z * f}; }
inline Vec3 operator/(Vec3 a, f32 f) { return Vec3{a.x / f, a.y / f, a.z / f}; }

//~ NOTE(samuel): f32 Vec3 operations
inline Vec3 operator+(f32 f, Vec3 a) { return Vec3{f + a.x, f + a.y, f + a.z}; }
inline Vec3 operator-(f32 f, Vec3 a) { return Vec3{f - a.x, f - a.y, f - a.z}; }
inline Vec3 operator*(f32 f, Vec3 a) { return Vec3{f * a.x, f * a.y, f * a.z}; }
inline Vec3 operator/(f32 f, Vec3 a) { return Vec3{f / a.x, f / a.y, f / a.z}; }


//~ NOTE(samuel): Vec3i Vec3i operations
inline Vec3i operator+(Vec3i a, Vec3i b) { return Vec3i{a.x + b.x, a.y + b.y, a.z + b.z}; }
inline Vec3i operator-(Vec3i a, Vec3i b) { return Vec3i{a.x - b.x, a.y - b.y, a.z - b.z}; }
inline Vec3i operator*(Vec3i a, Vec3i b) { return Vec3i{a.x * b.x, a.y * b.y, a.z * b.z}; }
inline Vec3i operator/(Vec3i a, Vec3i b) { return Vec3i{a.x / b.x, a.y / b.y, a.z / b.z}; }
inline b32 operator==(Vec3i a, Vec3i b) { return (a.x == b.x && a.y == b.y && a.z == b.z);}
inline b32 operator!=(Vec3i a, Vec3i b) { return (a.x != b.x && a.y != b.y && a.z != b.z);}

inline void operator+=(Vec3i &a, Vec3i b) { a = a + b; }
inline void operator-=(Vec3i &a, Vec3i b) { a = a - b; }
inline void operator*=(Vec3i &a, Vec3i b) { a = a * b; }
inline void operator/=(Vec3i &a, Vec3i b) { a = a / b; }

//~ NOTE(samuel): Vec3i i32 operations
inline Vec3i operator+(Vec3i a, i32 f) { return Vec3i{a.x + f, a.y + f, a.z + f}; }
inline Vec3i operator-(Vec3i a, i32 f) { return Vec3i{a.x - f, a.y - f, a.z - f}; }
inline Vec3i operator*(Vec3i a, i32 f) { return Vec3i{a.x * f, a.y * f, a.z * f}; }
inline Vec3i operator/(Vec3i a, i32 f) { return Vec3i{a.x / f, a.y / f, a.z / f}; }

//~ NOTE(samuel): i32 Vec3i operations
inline Vec3i operator+(i32 f, Vec3i a) { return Vec3i{f + a.x, f + a.y, f + a.z}; }
inline Vec3i operator-(i32 f, Vec3i a) { return Vec3i{f - a.x, f - a.y, f - a.z}; }
inline Vec3i operator*(i32 f, Vec3i a) { return Vec3i{f * a.x, f * a.y, f * a.z}; }
inline Vec3i operator/(i32 f, Vec3i a) { return Vec3i{f / a.x, f / a.y, f / a.z}; }


//~ NOTE(samuel): Vec4 Vec4 operations
inline Vec4 operator+(Vec4 a, Vec4 b) { return Vec4{a.x + b.x, a.y + b.y, a.z + b.z, a.w + b.w}; }
inline Vec4 operator-(Vec4 a, Vec4 b) { return Vec4{a.x - b.x, a.y - b.y, a.z - b.z, a.w - b.w}; }
inline Vec4 operator*(Vec4 a, Vec4 b) { return Vec4{a.x * b.x, a.y * b.y, a.z * b.z, a.w * b.w}; }
inline Vec4 operator/(Vec4 a, Vec4 b) { return Vec4{a.x / b.x, a.y / b.y, a.z / b.z, a.w / b.w}; }

//~ NOTE(samuel): Vec4 f32 operations
inline Vec4 operator+(Vec4 a, f32 f) { return Vec4{a.x + f, a.y + f, a.z + f, a.w + f}; }
inline Vec4 operator-(Vec4 a, f32 f) { return Vec4{a.x - f, a.y - f, a.z - f, a.w - f}; }
inline Vec4 operator*(Vec4 a, f32 f) { return Vec4{a.x * f, a.y * f, a.z * f, a.w * f}; }
inline Vec4 operator/(Vec4 a, f32 f) { return Vec4{a.x / f, a.y / f, a.z / f, a.w / f}; }

//~ NOTE(samuel): f32 Vec4 operations
inline Vec4 operator+(f32 f, Vec4 a) { return Vec4{f + a.x, f + a.y, f + a.z, f + a.w}; }
inline Vec4 operator-(f32 f, Vec4 a) { return Vec4{f - a.x, f - a.y, f - a.z, f - a.w}; }
inline Vec4 operator*(f32 f, Vec4 a) { return Vec4{f * a.x, f * a.y, f * a.z, f * a.w}; }
inline Vec4 operator/(f32 f, Vec4 a) { return Vec4{f / a.x, f / a.y, f / a.z, f / a.w}; }

//~NOTE(samuel): Mat4 Functions

internal Mat4 mul(Mat4 b, Mat4 a)
{
    Mat4 r = {};
    
    r.x.x = a.x.x * b.x.x + a.x.y * b.y.x + a.x.z * b.z.x + a.x.w * b.w.x;
    r.x.y = a.x.x * b.x.y + a.x.y * b.y.y + a.x.z * b.z.y + a.x.w * b.w.y;
    r.x.z = a.x.x * b.x.z + a.x.y * b.y.z + a.x.z * b.z.z + a.x.w * b.w.z;
    r.x.w = a.x.x * b.x.w + a.x.y * b.y.w + a.x.z * b.z.w + a.x.w * b.w.w;
    
    r.y.x = a.y.x * b.x.x + a.y.y * b.y.x + a.y.z * b.z.x + a.y.w * b.w.x;
    r.y.y = a.y.x * b.x.y + a.y.y * b.y.y + a.y.z * b.z.y + a.y.w * b.w.y;
    r.y.z = a.y.x * b.x.z + a.y.y * b.y.z + a.y.z * b.z.z + a.y.w * b.w.z;
    r.y.w = a.y.x * b.x.w + a.y.y * b.y.w + a.y.z * b.z.w + a.y.w * b.w.w;
    
    r.z.x = a.z.x * b.x.x + a.z.y * b.y.x + a.z.z * b.z.x + a.z.w * b.w.x;
    r.z.y = a.z.x * b.x.y + a.z.y * b.y.y + a.z.z * b.z.y + a.z.w * b.w.y;
    r.z.z = a.z.x * b.x.z + a.z.y * b.y.z + a.z.z * b.z.z + a.z.w * b.w.z;
    r.z.w = a.z.x * b.x.w + a.z.y * b.y.w + a.z.z * b.z.w + a.z.w * b.w.w;
    
    r.w.x = a.w.x * b.x.x + a.w.y * b.y.x + a.w.z * b.z.x + a.w.w * b.w.x;
    r.w.y = a.w.x * b.x.y + a.w.y * b.y.y + a.w.z * b.z.y + a.w.w * b.w.y;
    r.w.z = a.w.x * b.x.z + a.w.y * b.y.z + a.w.z * b.z.z + a.w.w * b.w.z;
    r.w.w = a.w.x * b.x.w + a.w.y * b.y.w + a.w.z * b.z.w + a.w.w * b.w.w;
    
    return r;
}

inline Mat4 operator*(Mat4 b, Mat4 a) {return mul(b, a);}
inline void operator*=(Mat4 &b, Mat4 a) {b = mul(b, a);}

internal Mat4 transposed(Mat4 a)
{
    Mat4 r = {
        a.x.x, a.y.x, a.z.x, a.w.x,
        a.x.y, a.y.y, a.z.y, a.w.y,
        a.x.z, a.y.z, a.z.z, a.w.z,
        a.x.w, a.y.w, a.z.w, a.w.w,
    };
    return r;
}

internal Mat4 perspective_mat(f32 near, f32 far, f32 fov, f32 h2w)
{
    f32 _fov = fov * 3.1415926535 / 90.0;
    f32 S = dm_cos(_fov) / dm_sin(_fov);
    f32 s_h2w = S * h2w;
    f32 f1 = -(far / (far - near));
    f32 f2 = near * f1;
    
    Mat4 r = {
        -s_h2w, 0, 0, 0,
        0, -S, 0, 0,
        0, 0, f1, f2,
        0, 0, -1, 0,
    };
    
    return r;
}

inline f32 max(f32 a, f32 b) {return (a < b) ? b : a;}
inline f32 min(f32 a, f32 b) {return (a > b) ? b : a;}

inline i32 max(i32 a, i32 b) {return (a < b) ? b : a;}
inline i32 min(i32 a, i32 b) {return (a > b) ? b : a;}

#define clamp(n, a, b) max((b), min((n), (a)));

inline f32 lerp(f32 a, f32 b, f32 t) {
    return (1 - t) * a + t * b;
}
inline Vec2 lerp(Vec2 a, Vec2 b, f32 t) {
    return Vec2{lerp(a.x, b.x, t), lerp(a.y, b.y, t)};
}
inline Vec3 lerp(Vec3 a, Vec3 b, f32 t) {
    return Vec3{lerp(a.x, b.x, t),lerp(a.y, b.y, t),lerp(a.z, b.z, t)};
}
inline Vec4 lerp(Vec4 a, Vec4 b, f32 t) {
    return Vec4{
        lerp(a.x, b.x, t),lerp(a.y, b.y, t),
        lerp(a.z, b.z, t),lerp(a.w, b.w, t)
    };
}

internal f32
smoothstep(f32 a, f32 b, f32 w)
{
    if (0.0 > w) return a;
    if (1.0 < w) return b;
    
    //return (a1 - a0) * w + a0;
    return (b - a) * (3.0 - w * 2.0) * w * w + a;
}


inline f32 dot(Vec2 a, Vec2 b) {
    f32 f = a.x * b.x + a.y * b.y;
    return f;
}

inline f32 len2(Vec2 a) {
    f32 l = dot(a, a);
    return l;
}

inline f32 len(Vec2 a) {
    return dm_sqrt(len2(a));
}

inline f32 dot(Vec3 a, Vec3 b) {
    f32 f = a.x * b.x + a.y * b.y + a.z * b.z;
    return f;
}

inline f32 len2(Vec3 a) {
    f32 l = dot(a, a);
    return l;
}

inline f32 len(Vec3 a) {
    return dm_sqrt(len2(a));
}

inline f32 magnitude(Vec2 v) {
    return dm_sqrt(dot(v, v));
}

inline Vec2 normalize(Vec2 v) {
    f32 len = dm_sqrt(dot(v, v));
    if (len == 0.0) return (Vec2){};
    return v / len;
}

inline Vec3 normalize(Vec3 v) {
    f32 len = dm_sqrt(dot(v, v));
    if (len == 0.0) return (Vec3){};
    return v / len;
}

// TODO(samuel): Optimize perlin noise later
internal Vec2
random_gradient(Vec2i i)
{
    auto v = vec2(i);
    
    auto random = 2920.0 * dm_sin(v.x * 21942.0 + v.y * 171324.0 + 8912.0) *
        dm_cos(v.x * 23157.0 * v.y * 217832.0 + 9758.0);
    return vec2(dm_cos(random), dm_sin(random));
}

internal f32
dot_grid_gradient(Vec2i i, Vec2 f)
{
    Vec2 grad = random_gradient(i);
    Vec2 d = f - vec2(i);
    return dot(grad, d);
}

internal f32
perlin(Vec2 v)
{
    Vec2i v0 = vec2i(v);
    Vec2i v1 = v0 + 1;
    
    Vec2 s = v - vec2(v0);
    
    f32 n0  = dot_grid_gradient(v0, v);
    f32 n1  = dot_grid_gradient(vec2i(v1.x, v0.y), v);
    f32 ix0 = smoothstep(n0, n1, s.x);
    
    n0 = dot_grid_gradient(vec2i(v0.x, v1.y), v);
    n1 = dot_grid_gradient(v1, v);
    f32 ix1 = smoothstep(n0, n1, s.x);
    
    f32 value = smoothstep(ix0, ix1, s.y);
    
    return value;
}


// ==== Collision =====

enum DShapeType {
    DShapeType_Rect,
    DShapeType_Circle,
    DShapeType_Line,
};

struct DCircle {
    Vec2 center;
    float radius;
};

struct DRect {
    Vec2 position;
    Vec2 size;
};

struct DLine {
    Vec2 p0;
    Vec2 p1;
};

struct DShape {
    DShapeType type;
    union {
        DCircle c;
        DRect   r;
        DLine   l;
    };
};

inline Vec2
project(Vec2 u, Vec2 v) {
    return (dot(u,v) / dot(u, u)) * u;
}

internal Vec2
project_on_line(Vec2 p, DLine l) {
    Vec2 u = l.p1 - l.p0;
    Vec2 v = p - l.p0;
    return l.p0 + project(u, v);
}

internal Vec2
project_on_line_clamped(Vec2 p, DLine l) {

    Vec2 u = l.p1 - l.p0;
    Vec2 v = project_on_line(p, l);
    Vec2 w = v - l.p0;

    if (dot(u, w) > dot(u, u)) return l.p1;
    if (dot(u, w) < 0)         return l.p0;

    return v;
}

struct DRectVertices {
    Vec2 points[4];
};

struct DRectEdges {
    DLine lines[4];
};

internal DRectVertices
vertices_aligned(DRect r) {
    DRectVertices vs;

    f32 x = r.size.x * 0.5;
    f32 y = r.size.y * 0.5;

    Vec2 offset = vec2(x, y);

    vs.points[0] = r.position - offset;
    vs.points[1] = vs.points[0] + vec2(0, r.size.y);
    vs.points[2] = vs.points[1] + vec2(r.size.x, 0);
    vs.points[3] = vs.points[0] + vec2(r.size.x, 0);

    return vs;
}

internal DRectEdges
edges_aligned(DRect r) {
    DRectEdges es;

    auto vs = vertices_aligned(r);

    es.lines[0] = (DLine){vs.points[0], vs.points[1]};
    es.lines[1] = (DLine){vs.points[1], vs.points[2]};
    es.lines[2] = (DLine){vs.points[2], vs.points[3]};
    es.lines[3] = (DLine){vs.points[3], vs.points[0]};

    return es;
}

internal Vec2
center_aligned(DRect r) {
    auto vs = vertices_aligned(r);

    auto x = vs.points[3] - vs.points[0];
    auto y = vs.points[1] - vs.points[0];

    return vs.points[0] + (x + y) * 0.5;
}

internal bool
collision(Vec2 p, DLine l, f32 tolerance = 0.001) {
    f32 a = len2(p - l.p0) + len2(p - l.p1) - tolerance;
    f32 b = len2(l.p1 - l.p0);
    bool result = a <= b;
    return result;
}

internal bool
collision(Vec2 p, DCircle c) {
    bool result = len2(c.center - p) <= (c.radius * c.radius);
    return result;
}

internal bool
collision(Vec2 p, DRect r) {
    f32 x = r.size.x * 0.5;
    f32 y = r.size.y * 0.5;

    bool result = (p.x >= r.position.x - x) &&
                  (p.x <= r.position.x + r.size.x - x) &&
                  (p.y >= r.position.y - y) &&
                  (p.y <= r.position.y + r.size.y - y);
    return result;
}

internal bool
collision(DLine l1, DLine l2) {
    Vec2 v1 = l1.p1 - l1.p0;
    Vec2 v2 = l2.p1 - l2.p0;
    Vec2 v3 = l1.p0 - l2.p0;

    f32 den = v1.x * v2.y - v1.y * v2.x;

    if (den == 0) {
        return collision(l1.p0, l2, 0) || collision(l1.p1, l2, 0);
    }

    f32 a1 = (v1.x*v3.x - v1.y*v3.x) / den;
    f32 a2 = (v2.x*v3.y - v2.y*v3.x) / den;

    return a1 >= 0 && a1 <= 1 && a2 >= 0 && a2 <= 1;
}

internal bool
collision(DLine l, DCircle c) {
    Vec2 p = project_on_line_clamped(c.center, l);
    return collision(p, c);
}
inline bool collision(DCircle c, DLine l) {return collision(l, c);}

internal bool
collision(DLine l, DRect r) {
    if (collision(l.p0, r) || collision(l.p1, r)) {
        return true;
    }

    auto es = edges_aligned(r);

    for (int i = 0; i < 4; i++) {
        if (collision(l, es.lines[i])) {
            return true;
        }
    }

    return false;
}
inline bool collision(DRect r, DLine l) {return collision(l, r);}

internal bool
collision(DCircle c1, DCircle c2) {
    f32 r2 = c1.radius + c2.radius;
    f32 l  = len2(c1.center - c2.center);
    return l <= r2*r2;
}

internal bool
collision(DCircle c, DRect r) {
    auto rc = center_aligned(r);
     
    f32 a = c.radius*(c.radius + r.size.x + r.size.y) + (r.size.x*r.size.x) + (r.size.y*r.size.y) / 4.0f;
    if (dot(rc - c.center, rc - c.center) >= a) {
        return false;
    }

    if (collision(c.center, r) || collision(rc, c)) {
        return true;
    }

    auto es = edges_aligned(r);

    for (int i = 0; i < 4; i++) {
        auto p = project_on_line_clamped(c.center, es.lines[i]);
        if (collision(p, c)) {
            return true;
        }
    }

    return false;
}
inline bool collision(DRect r, DCircle c) {return collision(c, r);}

internal bool
collision(DRect r1, DRect r2) {
    auto x1 = r1.size.x * 0.5;
    auto y1 = r1.size.y * 0.5;

    auto x2 = r2.size.x * 0.5;
    auto y2 = r2.size.y * 0.5;

    return (r1.position.x + r1.size.x - x1) >= (r2.position.x - x2) &&
           (r1.position.y + r1.size.y - y1) >= (r2.position.y - y2) &&
           (r1.position.x - x1)             <= (r2.position.x + r2.size.x - x2) &&
           (r1.position.y - y1)             <= (r2.position.y + r2.size.y - y2);
}

internal bool
collision(DShape s1, DShape s2) {
    switch (s1.type) {
        case DShapeType_Line: {
            switch(s2.type) {
                case DShapeType_Line:   return collision(s1.l, s2.l); break;
                case DShapeType_Circle: return collision(s1.l, s2.c); break;
                case DShapeType_Rect:   return collision(s1.l, s2.r); break;
            } 
        } break;

        case DShapeType_Circle: {
            switch(s2.type) {
                case DShapeType_Line:   return collision(s1.c, s2.l); break;
                case DShapeType_Circle: return collision(s1.c, s2.c); break;
                case DShapeType_Rect:   return collision(s1.c, s2.r); break;
            } 
        } break;

        case DShapeType_Rect: {
            switch(s2.type) {
                case DShapeType_Line:   return collision(s1.r, s2.l); break;
                case DShapeType_Circle: return collision(s1.r, s2.c); break;
                case DShapeType_Rect:   return collision(s1.r, s2.r); break;
            } 
        } break;
    }
}

#endif //DMATH_H
