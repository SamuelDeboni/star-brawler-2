internal void
wave_update() {

	if (gs->wave == 0 && gs->wave_timer > 3) {
		spawn_positioning_seeker(vec2(460, 125), vec2(430, 125), 1);
		spawn_positioning_seeker(vec2(460, 225), vec2(430, 225), 1);
		spawn_positioning_seeker(vec2(460,  25), vec2(430, 	25), 1);

		gs->wave++;	

		#if 0
		gs->level = 1;
		gs->change_background = true;
		gs->bg3_scroll_x = -900;
		gs->bg4_scroll_x = -900;
		#endif
	} else if (gs->wave == 1 && gs->wave_timer > 10) {
		for(int i=0; i<3; i++){
			spawn_positioning_seeker(vec2(480 + 75*i, 175), vec2(430, 175), 1);
			spawn_positioning_seeker(vec2(470 + 75*i, 150), vec2(410, 150), 1);
			spawn_positioning_seeker(vec2(460 + 75*i, 125), vec2(400, 125), 1);
			spawn_positioning_seeker(vec2(470 + 75*i, 100), vec2(410, 100), 1);
			spawn_positioning_seeker(vec2(460 + 75*i,  75), vec2(430,  75), 1);
		}

		gs->wave++;	
	} else if (gs->wave == 2 && gs->wave_timer > 20) {
		spawn_basic_ship(vec2(470, 175), 1);
		spawn_basic_ship(vec2(460, 125), 1);
		spawn_basic_ship(vec2(470,  75), 1);

		gs->wave++;	
	} else if (gs->wave == 3 && gs->wave_timer > 30) {
		for(int i=0; i<2; i++){
			spawn_basic_ship(vec2(480 + 90*i, 175), 1);
			spawn_basic_ship(vec2(480 + 90*i, 150), 1);
			spawn_basic_ship(vec2(460 + 90*i, 125), 1);
		}
		spawn_positioning_seeker(vec2(400, 300 + 100), vec2(400, 75 + 100), 1);
		spawn_positioning_seeker(vec2(400, 300 +  75), vec2(400, 75 +  75), 1);
		spawn_positioning_seeker(vec2(400, 300 +  50), vec2(400, 75 +  50), 1);
		spawn_positioning_seeker(vec2(400, 300 +  25), vec2(400, 75 +  25), 1);
		spawn_positioning_seeker(vec2(400, 300 +   0), vec2(400, 75 +  0), 1);

		gs->wave++;
			
	} else if (gs->wave == 4 && gs->wave_timer > 45) {
		spawn_cluster(vec2(480, 125), 1.2);

		gs->wave++;

	} else if (gs->wave == 4 && gs->wave_timer > 45) {
		spawn_cluster(vec2(480, 125), 1.2);

		gs->wave++;

	} 

	else if (gs->wave == 5 && gs->wave_timer > 50) {
		spawn_cluster(vec2(460, 180), 1);
		spawn_cluster(vec2(460,  70), 1);

		gs->wave++;

	} else if (gs->wave == 6 && gs->wave_timer > 55) {
		spawn_cluster(vec2(470, 200), 1);
		spawn_cluster(vec2(460, 125), 1);
		spawn_cluster(vec2(470,  50), 1);

		gs->wave++;

	} 

	else if (gs->wave == 7 && gs->wave_timer > 60) {
		spawn_positioning_seeker(vec2(50, 300 + 100), vec2(50, 75 + 100), 1);
		spawn_positioning_seeker(vec2(50, 300 +  75), vec2(50, 75 +  75), 1);
		spawn_positioning_seeker(vec2(50, 300 +  50), vec2(50, 75 +  50), 1);
		spawn_positioning_seeker(vec2(50, 300 +  25), vec2(50, 75 +  25), 1);
		spawn_positioning_seeker(vec2(50, 300 +   0), vec2(50, 75 +   0), 1);

		spawn_basic_ship(vec2(480, 185), 1);
		spawn_basic_ship(vec2(460, 150), 1);
		spawn_basic_ship(vec2(460, 100), 1);
		spawn_basic_ship(vec2(480,  65), 1);

		gs->wave++;
	}

	else if (gs->wave == 8 && gs->wave_timer > 70) {
		spawn_basic_ship(vec2(480, 200), 1);
		spawn_basic_ship(vec2(460, 160), 1);
		spawn_basic_ship(vec2(460, 125), 1);
		spawn_basic_ship(vec2(460,  90), 1);
		spawn_basic_ship(vec2(480,  50), 1);
		
		gs->wave++;
	} else if (gs->wave == 9 && gs->wave_timer > 73) {
		spawn_cluster(vec2(460, 125), 1.2);
		
		gs->wave++;
	} else if (gs->wave == 10 && gs->wave_timer > 76) {
		spawn_cluster(vec2(460, 125 + 80), 1.2);
		
		gs->wave++;
	} else if (gs->wave == 11 && gs->wave_timer > 80) {
		spawn_cluster(vec2(460, 125 - 80), 1.2);
		
		gs->wave++;
	} else if (gs->wave == 12 && gs->wave_timer > 85) {
		spawn_cluster(vec2(460, 125 + 40), 1.2);
		spawn_cluster(vec2(460, 125 - 40), 1.2);
		
		gs->wave++;
	}

	else if (gs->wave == 13 && gs->wave_timer > 95) {
		for(int i=0; i<3; i++){
			spawn_positioning_seeker(vec2(460 + 100*i, 130 + 100), vec2(430, 130 + 100), 1);
			spawn_positioning_seeker(vec2(460 + 100*i, 130 +  50), vec2(430, 130 +  50), 1);
			spawn_positioning_seeker(vec2(460 + 100*i, 130 +   0), vec2(430, 130 +   0), 1);
			spawn_positioning_seeker(vec2(460 + 100*i, 110 -   0), vec2(430, 110 -   0), 1);
			spawn_positioning_seeker(vec2(460 + 100*i, 110 -  50), vec2(430, 110 -  50), 1);
			spawn_positioning_seeker(vec2(460 + 100*i, 110 - 100), vec2(430, 110 - 100), 1);
		}
		
		gs->wave++;
	} else if (gs->wave == 14 && gs->wave_timer > 105) {
		spawn_cluster(vec2(460, 125 + 40), 1.2);
		
		gs->wave++;
	} else if (gs->wave == 15 && gs->wave_timer > 115) {
		spawn_cluster(vec2(460, 125 - 40), 1.2);
		
		gs->wave++;
	}


	else if (gs->wave == 16 && gs->wave_timer > 125) {
		gs->mini_boss.disabled = false;
		
		gs->wave++;
	}

	gs->wave_timer += delta;
}

internal void
wave2_update() {
	if (gs->wave == 0 && gs->wave_timer > 3) {
		spawn_laser_bit(vec2(460, 125), 1);

		gs->wave++;	

		#if 0
		gs->level = 1;
		gs->change_background = true;
		gs->bg3_scroll_x = -900;
		gs->bg4_scroll_x = -900;
		#endif
	} else if (gs->wave == 1 && gs->wave_timer > 10) {
		spawn_laser_bit(vec2(460, 175), 1);

		gs->wave++;	
	} else if (gs->wave == 2 && gs->wave_timer > 12) {
		spawn_laser_bit(vec2(460, 75), 1);

		gs->wave++;	
	} 
	else if (gs->wave == 3 && gs->wave_timer > 20) {
		spawn_laser_bit(vec2(460, 225), 1);
		spawn_positioning_seeker(vec2(480, 175), vec2(430, 175), 1);
		spawn_positioning_seeker(vec2(470, 150), vec2(410, 150), 1);
		spawn_positioning_seeker(vec2(460, 125), vec2(400, 125), 1);
		spawn_positioning_seeker(vec2(470, 100), vec2(410, 100), 1);
		spawn_positioning_seeker(vec2(460,  75), vec2(430,  75), 1);
		spawn_laser_bit(vec2(460,  25), 1);

		gs->wave++;
			
	} 
	else if (gs->wave == 4 && gs->wave_timer > 35) {
		spawn_basic_ship(vec2(480, 200), 1);
		spawn_basic_ship(vec2(460, 160), 1);
		spawn_laser_bit(vec2(460, 125), 1);
		spawn_basic_ship(vec2(460,  90), 1);
		spawn_basic_ship(vec2(480,  50), 1);

		gs->wave++;

	} 

	else if (gs->wave == 5 && gs->wave_timer > 45) {
		spawn_boomerang(vec2(460, 175), 1);
		spawn_boomerang(vec2(460, 75), 1);

		gs->wave++;

	} else if (gs->wave == 6 && gs->wave_timer > 55) {
		spawn_boomerang(vec2(480, 225), 1);
		spawn_boomerang(vec2(460, 175), 1);
		spawn_boomerang(vec2(460,  75), 1);
		spawn_boomerang(vec2(480,  25), 1);
		gs->wave++;

	} 

	else if (gs->wave == 7 && gs->wave_timer > 70) {
		spawn_boomerang(vec2(460, 200), 1);
		spawn_laser_bit(vec2(460, 160), 1);
		spawn_boomerang(vec2(460, 125), 1);
		spawn_laser_bit(vec2(460,  90), 1);
		spawn_boomerang(vec2(460,  50), 1);

		gs->wave++;
	}

	else if (gs->wave == 8 && gs->wave_timer > 80) {
		spawn_basic_ship(vec2(480, 200), 1);
		spawn_basic_ship(vec2(460, 160), 1);
		spawn_basic_ship(vec2(460, 125), 1);
		spawn_basic_ship(vec2(460,  90), 1);
		spawn_basic_ship(vec2(480,  50), 1);
		
		gs->wave++;
	} else if (gs->wave == 9 && gs->wave_timer > 85) {
		spawn_cluster(vec2(460, 125), 1.2);
		spawn_laser_bit(vec2(460,  200), 1);
		
		gs->wave++;
	} else if (gs->wave == 10 && gs->wave_timer > 90) {
		spawn_laser_bit(vec2(460,  50), 1);
		
		gs->wave++;
	} else if (gs->wave == 11 && gs->wave_timer > 95) {
		spawn_boomerang(vec2(480, 175), 1);
		spawn_boomerang(vec2(460, 125), 1);
		
		gs->wave++;
	} else if (gs->wave == 12 && gs->wave_timer > 100) {
		spawn_cluster(vec2(460, 125 + 40), 1.2);
		spawn_cluster(vec2(460, 125 - 40), 1.2);
		
		gs->wave++;
	}

	else if (gs->wave == 13 && gs->wave_timer > 115) {
		spawn_positioning_seeker(vec2(50, 300 + 100), vec2(50, 75 + 100), 1);
		spawn_positioning_seeker(vec2(50, 300 +  75), vec2(50, 75 +  75), 1);
		spawn_positioning_seeker(vec2(50, 300 +  50), vec2(50, 75 +  50), 1);
		spawn_positioning_seeker(vec2(50, 300 +  25), vec2(50, 75 +  25), 1);
		spawn_positioning_seeker(vec2(50, 300 +   0), vec2(50, 75 +   0), 1);

		spawn_boomerang(vec2(480, 225), 1);
		spawn_boomerang(vec2(480,  25), 1);

		gs->wave++;
	} else if (gs->wave == 14 && gs->wave_timer > 120) {
		spawn_positioning_seeker(vec2(400, 300 + 100), vec2(400, 75 + 100), 1);
		spawn_positioning_seeker(vec2(400, 300 +  75), vec2(400, 75 +  75), 1);
		spawn_positioning_seeker(vec2(400, 300 +  50), vec2(400, 75 +  50), 1);
		spawn_positioning_seeker(vec2(400, 300 +  25), vec2(400, 75 +  25), 1);
		spawn_positioning_seeker(vec2(400, 300 +   0), vec2(400, 75 +   0), 1);

		spawn_laser_bit(vec2(480, 225), 1);
		spawn_laser_bit(vec2(460, 125), 1);
		spawn_laser_bit(vec2(480,  25), 1);

		gs->wave++;
	} else if (gs->wave == 15 && gs->wave_timer > 125) {
		spawn_positioning_seeker(vec2(50, 300 + 100), vec2(50, 75 + 100), 1);
		spawn_positioning_seeker(vec2(50, 300 +  75), vec2(50, 75 +  75), 1);
		spawn_positioning_seeker(vec2(50, 300 +  50), vec2(50, 75 +  50), 1);
		spawn_positioning_seeker(vec2(50, 300 +  25), vec2(50, 75 +  25), 1);
		spawn_positioning_seeker(vec2(50, 300 +   0), vec2(50, 75 +   0), 1);

		spawn_cluster(vec2(460, 225), 1);
		spawn_cluster(vec2(460, 125), 1);
		spawn_cluster(vec2(460,  25), 1);
		
		gs->wave++;
	} else if (gs->wave == 15 && gs->wave_timer > 130) {
		spawn_positioning_seeker(vec2(400, 300 + 100), vec2(400, 75 + 100), 1);
		spawn_positioning_seeker(vec2(400, 300 +  75), vec2(400, 75 +  75), 1);
		spawn_positioning_seeker(vec2(400, 300 +  50), vec2(400, 75 +  50), 1);
		spawn_positioning_seeker(vec2(400, 300 +  25), vec2(400, 75 +  25), 1);
		spawn_positioning_seeker(vec2(400, 300 +   0), vec2(400, 75 +   0), 1);

		spawn_boomerang(vec2(460, 225), 1);
		spawn_boomerang(vec2(460, 125), 1);
		spawn_boomerang(vec2(460,  25), 1);
		
		gs->wave++;
	} else if (gs->wave == 16) {
		if (gs->wave_timer > 150) {
			gs->wave = 0;
			gs->level++;

			start_boss(true);

			gs->current_music = LoadMusicStream("assets/music/BossMain.wav");
		    PlayMusicStream(gs->current_music);
		}
	}

	gs->wave_timer += delta;
}
