#!/bin/sh

# setup emsdk enviroment
ENV_SCRIPT='/home/samuel/.local/opt/emsdk/emsdk_env.sh'
source $ENV_SCRIPT

# project parameters
name='game'
main='main'
memory=67108864
assets_dir='assets'

rm web/*

# build command
#emcc -std=c++11 src/$main.cpp -o web/$name.html ./lib/libraylib_web.a -Os -Wall -Iinclude -Llib -s USE_GLFW=3 -s ASYNCIFY --shell-file ./lib/shell.html --preload-file $assets_dir -s TOTAL_MEMORY=$memory
emcc -std=c++11 src/$main.cpp -o web/$name.html ./lib/libraylib_web.a -Os -DPLATFORM_WEB=1 -Wall -Iinclude -Llib -s USE_GLFW=3 -s --shell-file ./lib/shell.html --preload-file $assets_dir -s TOTAL_MEMORY=$memory

# run test
if [[ $1 = 'run' ]] ; then
	emrun web/$name.html --browser firefox
fi

# generate itchio release
if [[ $1 = 'release' ]] ; then
	mv web/$name.html web/index.html
	zip $name.zip ./web/*
fi
