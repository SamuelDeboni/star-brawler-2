#!/bin/sh

# project parameters
name='sb2'
main='main'

rm $name

# build command
clang -std=c++11 -g3 src/$main.cpp -o $name ./lib/libraylib.a -Iinclude -lGL -lm -lpthread -ldl -lrt -lX11

if [[ $1 = 'run' ]] ; then
	./$name
fi
