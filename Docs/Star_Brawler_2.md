# Star Brawler 2

## Basic concept

A sequel for the critically acclaimed star brawler. Featuring a melee fighting ship, counterfields and energy management.

## Main mechanics

The player controls a retronian ship battling waves of enemies using mainly melee atacks, plus the addition of a counterfield, a field surrounding the ship that absorbs energy from enemy projectiles that cross it.  By charging the counter field, the player can spend the energy in special moves. Additionally, the ship can still grab weak ships and weak projectiles. 

* Main Attack

  The ship has 4 mechanical tentacles. Those tentacles are used to quickly punch enemies ship at close range.

* Counter Field

  A spherical field surrounding the ship that absorbs energy from enemy projectiles that passes through it. The energy absorbed is used to take damage and use special abilities.

* Energy

  The ship's energy represents it's health and is used in special abilities. The player can only recharge the ship's energy by using the counter field or punching enemies.

* Grab

  The player can grab weak enemies and weak projectiles. Projectiles can be weakened when passing through the counter field. Anything grabbed by the ship can be thrown to damage enemies; 

* Abilities

  Special combat moves to help the player to defeat the horde of enemies.

  * Flare up

    Redirects energy to the extremes of the tentacles to increase the damage and range of the punches for a time.

  * Shield Spin

    The ship spins with energized tentacles and damages surrounding enemies while destroying projectiles;

  * B.F.L

    Big Fucking Laser, self explanatory;

## Enemies

* Auric Seeker

  Small cubic enemy that appears in swarms. Moments after appearing, they charge at the player with slight homing, despawning when off screen. Low health, can be grabbed at full health.

  * Damage: 10

  * Hit points: 1

* Auric Ship

  A basic, small, battle ship. They come from the left of the screen then, stop at a certain X value, then attempt to align with the player's Y position, stop then shoot one basic projectile. They positon and fire 3 times before advancing to a new X position.

  * Damage: 5

  * Hit points: 5

* Seeker Cluster

  A cluster time bomb that, upon exploding, releases 10 basic projecitles and scatters 5 auric seekers. It can be destroyed or grabbed before exploding.

  * Damage: 20

  * Hit points: 20

## Projecitles

* Basic projectile

  A small plasma ball that travels in a straight line.

  * Damage: 5

    
